Copy and paste commands one at a time into bash shell.

# --- Get root! ---
sudo su

# --- install the text editor joe ---
apt install joe

# --- install GCC
apt install gcc build-essential

# --- get boost libs ---
apt install libboost-all-dev
apt install git-core
apt update
apt upgrade
apt dist-upgrade
reboot

# --- get SDL libs ---
apt install libsdl1.2-dev

# --- or get SDL2 libs
apt install libsdl2-dev

usermod -a -G video pi

# --- install samba ---
apt install samba samba-common-bin

# --- make a place for source code.
mkdir /home/pi/code
chown pi.pi /home/pi/code
chmod 777 /home/pi/code

# --- configure samba to share this place on a Windows workgroup share.
joe /etc/samba/smb.conf


Look for

workgroup = WORKGROUP
# edit this to be the name of your own Windows workgroup.




# Uncomment
security = user


At the end of the text file smb.conf, add the following lines:

#--------------------------
[code]
comment = Pi code
path = /home/pi/code
writeable = Yes
only guest = No
create mask = 0777
directory mask = 0777
browseable = Yes
public = yes
#--------------------------





# --- restart and test the samba situation ---
service samba restart
testparm -s




# --- add the user pi to the smbpasswd file
smbpasswd -a pi




Now, from a Windows machine, you should be able to browse your Network Neighborhood and find the Pi! You should be able to open a folder window on the Pi and drag and drop stuff there. It will end up in /home/pi/code

Copy the latest version of the LaserBoy zip there and unzip it. You do not need to be root to do this.
https://laserboy.org/code/LaserBoy_Current.zip

# --- as the user pi, in a command prompt, navigate to /home/pi/code ---
unzip LaserBoy_Current.zip


To build the LaserBoy executable for the Pi, navigate to /home/pi/code/LaserBoy/src in a command prompt (either native or via SSH) and issue the following command:

time make -f Makefile_sdl1.rpi

This makes a version of LaserBoy that links with SDL 1.2.15. This version can be run from a command prompt with no X server running if the Pi kernel is built with frame buffer support.
The executable it builds will be called LaserBoy_sdl1_rpi

~ or ~

time make -f Makefile.rpi

This makes a version of LaserBoy that links with SDL2. This version can only be run from inside a running X server.
The executable it builds will be called LaserBoy_rpi

Both options will build all of the object files and link them together. It will also tell you exactly how long that process took.

Once the compile is done, back out of the src directory and issue the command

To run LaserBoy_sdl1_rpi from a command prompt...

fbset

Note the resolution of the Linux Frame Buffer!

mode "1280x1024"
    geometry 1280 1024 1280 1024 16
    timings 0 0 0 0 0 0 0
    rgba 5/11,6/5,5/0,0/16
endmode

This example screen is 1280 pixels wide, 1024 high and each pixel takes 16 bits. The pixel resolution of the screen was chosen when the HDMI port detected the monitor. This is a 19 inch LCD monitor. Fortunately, it is not at all necessary to change the pixel resolution of the video system. The only thing that is of interests to change is the bits-per-pixel or color depth of the screen.

fbset -depth 24

This command sets the video display to be full 8 bits for each color channel of RGB.

The Pi supports 8, 16, 24 and 32 bits-per-pixel. LaserBoy runs in the console in any of these color depths. LaserBoy does not change the color depth. It uses it as it was set with fbset.

./LaserBoy_sdl1_rpi 1280 1024

Will start LaserBoy in the whole screen of the console at native monitor resolution. You can also specify resolution that are smaller areas and the Pi video system will do its best to fill the screen. Don't try values that are larger than your screen. They either don't work at all or they can crash your video causing you to have to reboot your Pi.

LaserBoy_sdl1_rpi also runs in a window in the X GUI system. Go into the graphical desktop environment by issuing the command

startx

Open a terminal window and navigate to /home/pi/code/LaserBoy and issue the command

./LaserBoy_sdl1_rpi 1200 800

~ or ~

./LaserBoy_rpi 1200 800

In this command, the numbers 1200 800 are the width and height of the window that will open and be populated by the LaserBoy application. Feel free to adjust these values to suit your own particular needs. To exit the LaserBoy application, hit the [Esc] key and then answer y and y to the exit prompts.




By default The Pi is setup for 16 bit packed pixel video mode. If you want to see everything in 32 bit true color, you need to edit one of the files that get read before the Pi fully boots up. This is not necessary, as LaserBoy will run correctly in 16 bit packed pixel video. But, if you are adventurous and curious, this is how you do it.

# --- edit this file ---
joe /boot/config.txt


# --- add these lines to the file:
#--------------------------
framebuffer_depth=32
framebuffer_ignore_alpha=1
#--------------------------

reboot










If you have any questions, comments, suggestions or corrections, please feel free to contact me via email at james@akrobiz.com or join The LaserBoy Forum and send me a message.

http://laserboy.org/forum





4/14/2013
edit: 02/23/2015
edit: 08/22/2020
James Lehman.
