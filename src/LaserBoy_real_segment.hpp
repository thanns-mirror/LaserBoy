//############################################################################
//
// LaserBoy !!!
//
// by James Lehman
// Extra Stimulus Inc.
// james@akrobiz.com
//
// began: October 2003
//
// Copyright 2003 to 2020 James Lehman.
// This source is distributed under the terms of the GNU General Public License.
//
// LaserBoy_real_segment.hpp is part of LaserBoy.
//
// LaserBoy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LaserBoy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LaserBoy. If not, see <http://www.gnu.org/licenses/>.
//
//############################################################################
#ifndef __LASERBOY_REAL_SEGMENT_DEFINITIONS__
#define __LASERBOY_REAL_SEGMENT_DEFINITIONS__

//############################################################################
#include "LaserBoy_utility.hpp"
#include "LaserBoy_palette.hpp"
#include "LaserBoy_vertex.hpp"

//############################################################################
class LaserBoy_real_segment_set;
class LaserBoy_frame_set;
class LaserBoy_space;

//############################################################################
class LaserBoy_real_segment : public LaserBoy_real_segment_base
{
public:
    LaserBoy_real_segment(LaserBoy_space* ps)
                 : p_space            (ps                   ),
                   palette_index      (LASERBOY_ILDA_DEFAULT),
                   real_segment_error (LASERBOY_OK          )
                                                        {}
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space* ps,
                          const int& palette_index,
                          bool       add_origin
                         )
                 : p_space            (ps           ),
                   palette_index      (palette_index),
                   real_segment_error (LASERBOY_OK  )
                    {
                        if(add_origin)
                        {
                            push_back(LaserBoy_real_vertex());
                            push_back(LaserBoy_real_vertex());
                        }
                    }
    //------------------------------------------------------------------------
    LaserBoy_real_segment(const LaserBoy_real_segment& rs)
                 : p_space            (rs.p_space      ),
                   palette_index      (rs.palette_index),
                   real_segment_error (LASERBOY_OK     )
                    {
                        clear();
                        if(rs.size() > 1)
                        {
                            reserve(rs.size());
                            insert(begin(), rs.begin(), rs.end());
                        }
                    }
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*      ps,
                          LaserBoy_real_vertex from,
                          LaserBoy_real_vertex to
                         ); // 3D line function
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*      ps,
                          LaserBoy_real_vertex from,
                          LaserBoy_real_vertex to,
                          const u_int&         max_d
                         ); // 3D line function
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*           ps,
                          const LaserBoy_3D_double& p,
                          const LaserBoy_color      c
                         ); // 3D cross function
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*           ps,
                          LaserBoy_frame_set&       font_frames,
                          const string&             text,
                          const double              mono_font_space,
                          const bool                bond_word = false,
                          const bool                bond_line = false
                         );   // mono spaced font constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*           ps,
                          LaserBoy_frame_set&       font_frames,
                          const string&             text,
                          const double              vari_font_gap,
                          const double              vari_font_space,
                          const bool                bond_word = false,
                          const bool                bond_line = false
                         );   // variable spaced font constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*    ps,
                          const int          n,
                          const double       radius,
                          const double       pedals_numerator,
                          const double       pedals_denominator,
                          const double       start,
                          const double       duration
                         ); // uddddd rhodonea
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*    ps,
                          const double       center_radius,
                          const double       roller_radius,
                          const double       roller_offset,
                          const int          n,
                          const double       start,
                          const double       duration
                         ); // dddudd epitrochoid epicycloid
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*    ps,
                          const double       start,
                          const double       duration,
                          const double       center_radius,
                          const double       roller_radius,
                          const double       roller_offset,
                          const int          n
                         ); // dddddu hypotrochoid hypocycloid
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*    ps,
                          LaserBoy_pendulum  P1,
                          const int          n,
                          const double       duration
                         ); // pud pendulum constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*    ps,
                          const double       duration,
                          const int          n,
                          LaserBoy_pendulum  P1,
                          LaserBoy_pendulum  P2
                         ); // dupp pendulum_sum constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*    ps,
                          LaserBoy_pendulum  P1,
                          LaserBoy_pendulum  P2,
                          const int          n,
                          const double       duration
                         ); // ppud pendulum_xy constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*    ps,
                          LaserBoy_pendulum  P1,
                          LaserBoy_pendulum  P2,
                          LaserBoy_pendulum  P3,
                          const int          n,
                          const double       duration
                         ); // pppud pendulum_xyz constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*    ps,
                          LaserBoy_pendulum  P1,
                          LaserBoy_pendulum  P2,
                          LaserBoy_pendulum  P3,
                          LaserBoy_pendulum  P4,
                          const int          n,
                          const double       duration
                         ); // ppppud harmonograph constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*    ps,
                          LaserBoy_pendulum  P1,
                          LaserBoy_pendulum  P2,
                          LaserBoy_pendulum  P3,
                          LaserBoy_pendulum  P4,
                          LaserBoy_pendulum  P5,
                          LaserBoy_pendulum  P6,
                          const int          n,
                          const double       duration
                         ); // ppppppud harmonograph_3D constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*    ps,
                          const int          n,
                          const double       duration,
                          LaserBoy_pendulum  P1,
                          LaserBoy_pendulum  P2
                         ); // udpp amplitude_mod constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*    ps,
                          const int          n,
                          const double       duration,
                          LaserBoy_pendulum  P1,
                          LaserBoy_pendulum  P2,
                          LaserBoy_pendulum  P3,
                          LaserBoy_pendulum  P4
                         ); // udpppp amplitude_mod_xy constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*    ps,
                          const int          n,
                          const double       duration,
                          LaserBoy_pendulum  P1,
                          LaserBoy_pendulum  P2,
                          LaserBoy_pendulum  P3,
                          LaserBoy_pendulum  P4,
                          LaserBoy_pendulum  P5,
                          LaserBoy_pendulum  P6
                         ); // udpppppp amplitude_mod_xyz constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*    ps,
                          const int          n,
                          LaserBoy_pendulum  P1,
                          LaserBoy_pendulum  P2,
                          const double       duration
                         ); // uppd frequency_mod constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*    ps,
                          const int          n,
                          LaserBoy_pendulum  P1,
                          LaserBoy_pendulum  P2,
                          LaserBoy_pendulum  P3,
                          LaserBoy_pendulum  P4,
                          const double       duration
                         ); // uppppd frequency_mod_xy constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment(LaserBoy_space*    ps,
                          const int          n,
                          LaserBoy_pendulum  P1,
                          LaserBoy_pendulum  P2,
                          LaserBoy_pendulum  P3,
                          LaserBoy_pendulum  P4,
                          LaserBoy_pendulum  P5,
                          LaserBoy_pendulum  P6,
                          const double       duration
                         ); // uppppppd frequency_mod_xyz constructor
    //------------------------------------------------------------------------
virtual
   ~LaserBoy_real_segment() {}
    //------------------------------------------------------------------------
    bool is_2D() const
            {
                if(size() > 1)
                {
                    for(u_int i = 0; i < size(); i++)
                        if(at(i).z != 0.0)
                            return false;
                }
                return true;
            }
    //------------------------------------------------------------------------
    bool is_flat_in_x() const
            {
                if(size() > 1)
                {
                    double _x = first_lit_anchor().x;
                    for(u_int i = 1; i < size(); i++)
                        if(at(i).is_lit() && at(i).x != _x)
                            return false;
                }
                return true;
            }
    //------------------------------------------------------------------------
    bool is_flat_in_y() const
            {
                if(size() > 1)
                {
                    double _y = first_lit_anchor().y;
                    for(u_int i = 1; i < size(); i++)
                        if(at(i).is_lit() && at(i).y != _y)
                            return false;
                }
                return true;
            }
    //------------------------------------------------------------------------
    bool is_flat_in_z() const
            {
                if(size() > 1)
                {
                    double _z = first_lit_anchor().z;
                    for(u_int i = 1; i < size(); i++)
                        if(at(i).is_lit() && at(i).z != _z)
                            return false;
                }
                return true;
            }
    //------------------------------------------------------------------------
    bool operator == (const LaserBoy_real_segment& segment)
            {
                if(palette_index != segment.palette_index)
                    return false;
                if(size() != segment.size())
                    return false;
                for(u_int i = 0; i < size(); i++)
                    if(at(i) != segment.at(i))
                        return false;
                return true;
            }
    //------------------------------------------------------------------------
    LaserBoy_real_segment&  operator += (const LaserBoy_real_segment& segment)
                        {
                            insert(end(), segment.begin(), segment.end());
                            return *this;
                        }
    //------------------------------------------------------------------------
    LaserBoy_real_segment&  operator += (const LaserBoy_real_vertex& vertex)
                        {
                            push_back(vertex);
                            return *this;
                        }
    //------------------------------------------------------------------------
    LaserBoy_real_segment&  operator += (const LaserBoy_3D_double& float_3D)
                        {
                            for(u_int i = 0; i < size(); i++)
                                at(i) += float_3D;
                            return *this;
                        }
    //------------------------------------------------------------------------
    LaserBoy_real_segment&  operator -= (const LaserBoy_3D_double& float_3D)
                        {
                            for(u_int i = 0; i < size(); i++)
                                at(i) -= float_3D;
                            return *this;
                        }
    //------------------------------------------------------------------------
    LaserBoy_real_segment&  operator *= (const LaserBoy_3D_double& float_3D)
                        {
                            for(u_int i = 0; i < size(); i++)
                                at(i) *= float_3D;
                            return *this;
                        }
    //------------------------------------------------------------------------
    LaserBoy_real_segment   operator + (const LaserBoy_3D_double& float_3D)
                        {
                            LaserBoy_real_segment real_segment(p_space);
                            for(u_int i = 0; i < size(); i++)
                                real_segment.push_back(at(i) + float_3D);
                            return real_segment;
                        }
    //------------------------------------------------------------------------
    LaserBoy_real_segment   operator * (const LaserBoy_3D_double& float_3D)
                        {
                            LaserBoy_real_segment real_segment(p_space);
                            for(u_int i = 0; i < size(); i++)
                                real_segment.push_back(at(i) * float_3D);
                            return real_segment;
                        }
    //------------------------------------------------------------------------
    LaserBoy_real_segment&  operator =  (const LaserBoy_real_segment& rs)
                        {
                            p_space            = rs.p_space;
                            palette_index      = rs.palette_index;
                            real_segment_error = rs.real_segment_error;
                            clear();
                            if(rs.size() > 1)
                            {
                                reserve(rs.size());
                                insert(begin(), rs.begin(), rs.end());
                            }
                            return *this;
                        }
    //------------------------------------------------------------------------
    LaserBoy_real_segment   sub(LaserBoy_real_segment::const_iterator begin,
                                LaserBoy_real_segment::const_iterator last
                               )
                        {
                            LaserBoy_real_segment sub(p_space, palette_index, false);
                            sub.insert(sub.begin(), begin, last);
                            return sub;
                        }
    //------------------------------------------------------------------------
    LaserBoy_real_vertex  first_lit_vertex() const
                        {
                            for(u_int i = 1; i < size(); i++)
                                if(at(i).is_lit())
                                    return at(i);
                            return LaserBoy_real_vertex(0);
                        }
    //------------------------------------------------------------------------
    int first_lit_vector_index() const
                    {
                        for(u_int i = 1; i < size(); i++)
                            if(at(i).is_lit())
                                return i;
                        return -1;
                    }
    //------------------------------------------------------------------------
    LaserBoy_real_vertex first_lit_anchor() const
                    {
                        for(u_int i = 1; i < size(); i++)
                            if(at(i).is_lit())
                                return at(i - 1);
                        return LaserBoy_real_vertex(0);
                    }
    //------------------------------------------------------------------------
    int first_lit_anchor_index() const
                    {
                        for(u_int i = 1; i < size(); i++)
                            if(at(i).is_lit())
                                return (i - 1);
                        return -1;
                    }
    //------------------------------------------------------------------------
    LaserBoy_real_vertex last_lit_anchor() const
                    {
                        for(u_int i = size() - 1; i > 0; i--)
                            if(at(i).is_lit())
                                return at(i - 1);
                        return LaserBoy_real_vertex(0);
                    }
    //------------------------------------------------------------------------
    LaserBoy_real_vertex last_lit_vector() const
                    {
                        for(u_int i = size() - 1; i > 0; i--)
                            if(at(i).is_lit())
                                return at(i);
                        return LaserBoy_real_vertex(0);
                    }
    //------------------------------------------------------------------------
    int last_lit_vector_index() const
                    {
                        for(u_int i = size() - 1; i > 0; i--)
                            if(at(i).is_lit())
                                return i;
                        return -1;
                    }
    //------------------------------------------------------------------------
    u_int number_of_lit_vectors() const
                    {
                        u_int count = 0;
                        for(u_int i = 1; i < size(); i++)
                            if(at(i).is_lit())
                                count++;
                        return count;
                    }
    //------------------------------------------------------------------------
    u_int number_of_color_vectors() const;
    u_int number_of_dark_vectors () const;
    //------------------------------------------------------------------------
    u_int number_of_blank_vectors() const
                    {
                        u_int count = 0;
                        for(u_int i = 1; i < size(); i++)
                            if(at(i).is_blank())
                                count++;
                        return count;
                    }
    //------------------------------------------------------------------------
    u_int number_of_unique_colors() const
        {
            if(size() > 1)
            {
                u_int             i,
                                  j;
                LaserBoy_palette  palette(p_space);
                if(first_lit_vector_index() > -1)
                {
                    palette.push_back((LaserBoy_color)first_lit_vertex());
                    for(i = first_lit_vector_index() + 1; i < size(); i++)
                    {
                        for(j = 0; j < palette.number_of_colors(); j++)
                        {
                            if(palette[j] == (LaserBoy_color)at(i))
                                break;
                        }
                        if(    j == palette.number_of_colors() // color not found
                            && at(i).is_lit()
                          )
                            palette.push_back((LaserBoy_color)at(i));
                    }
                    return palette.number_of_colors();
                }
                return 0;
            }
        }
    //------------------------------------------------------------------------
    LaserBoy_real_segment& remove_vertex(u_int vertex_index)
            {
                if(size() < 3) // takes 2 vertices to make a vector!
                    clear();
                else if(vertex_index < size())
                {
                    u_int                 i;
                    LaserBoy_real_segment rs(p_space, palette_index, false);
                    rs.reserve(size());
                    for(i = 0; i < vertex_index; i++)
                        rs += at(i);
                    for(i = vertex_index + 1; i < size(); i++)
                        rs += at(i);
                    *this = rs;
                }
                return *this;
            }
    //------------------------------------------------------------------------
    LaserBoy_real_segment& sort()
            {
                if(size() < 2) // takes 2 vertices to make a vector!
                    clear();
                else
                    std::sort(begin(), end(), less_than);
                return *this;
            }
    //------------------------------------------------------------------------
    LaserBoy_3D_double      segment_front                 () const ;
    LaserBoy_3D_double      segment_back                  () const ;
    LaserBoy_3D_double      segment_top                   () const ;
    LaserBoy_3D_double      segment_bottom                () const ;
    LaserBoy_3D_double      segment_right                 () const ;
    LaserBoy_3D_double      segment_left                  () const ;
    double                  max_x                         () const ;
    double                  max_y                         () const ;
    double                  max_z                         () const ;
    double                  min_x                         () const ;
    double                  min_y                         () const ;
    double                  min_z                         () const ;
    double                  height                        () const ;
    double                  width                         () const ;
    double                  depth                         () const ;
    double                  vector_angle                  (u_int vertex) const ;
    double                  max_dimension                 () const ;
    double                  max_distance_from_origin      () const ;
    double                  max_distance_from_origin_xy   () const ;
    double                  max_distance_from_origin_zy   () const ;
    double                  max_distance_from_origin_xz   () const ;
    u_int                   i_max_distance_from_origin_xy () const ;
    u_int                   i_max_distance_from_origin_zy () const ;
    u_int                   i_max_distance_from_origin_xz () const ;
    double                  min_distance_from_origin      () const ;
    double                  min_distance_from_origin_xy   () const ;
    double                  min_distance_from_origin_zy   () const ;
    double                  min_distance_from_origin_xz   () const ;
    u_int                   i_min_distance_from_origin_xy () const ;
    u_int                   i_min_distance_from_origin_zy () const ;
    u_int                   i_min_distance_from_origin_xz () const ;
    bool                    segment_passes_through_origin (const double& granularity) const ;
    LaserBoy_real_segment&  reverse                       ();
    LaserBoy_real_segment&  reorder_from                  (u_int index);
    void                    blank_all_vertices            ();
    void                    unblank_all_vertices          ();
    void                    strip_color                   ();
    void                    strip_color_rgb               (const LaserBoy_color& c);
    void                    flip                          (u_int plane);
    void                    quarter_turn                  (u_int plane, u_int turns);
//    void                    z_order_vertices              (short span);
    void                    flatten_z                     ();
    void                    rotate                        (LaserBoy_3D_double a);
    void                    rotate_around_origin          (LaserBoy_3D_double a);
    //------------------------------------------------------------------------
    void                    rotate_on_coordinates         (LaserBoy_3D_double p,
                                                           LaserBoy_3D_double a
                                                          );
    void                    rotate_on_coordinates_x       (LaserBoy_3D_double p,
                                                           double             a
                                                          );
    void                    rotate_on_coordinates_y       (LaserBoy_3D_double p,
                                                           double             a
                                                          );
    void                    rotate_on_coordinates_z       (LaserBoy_3D_double p,
                                                           double             a
                                                          );
    //------------------------------------------------------------------------
    bool                    find_rgb_in_palette           (const LaserBoy_palette& palette);
    void                    set_rgb_from_palette          ();
    void                    set_palette_to_332            ();
    void                    sync_rgb_and_palette          ();
    void                    best_match_palette            (int index);
    void                    bit_reduce_to_palette         ();
    void                    best_reduce_to_palette        ();
    void                    convert_black_to_blank        ();
    void                    convert_blank_to_black        ();
    void                    impose_black_level            ();
    void                    reduce_blank_vectors          ();
    void                    reduce_lit_vectors            ();
    void                    omit_equivalent_vectors       ();
    LaserBoy_real_segment&  move                          (LaserBoy_3D_double d);
    LaserBoy_real_segment&  scale                         (LaserBoy_3D_double s);
    //------------------------------------------------------------------------
    LaserBoy_Error_Code     add_lit_span_vertices         ();
    void                    add_lit_span_vertices         (const u_int& max_d);
    LaserBoy_Error_Code     add_blank_span_vertices       ();
    //------------------------------------------------------------------------
    void                    scale_on_coordinates          (LaserBoy_3D_double p,
                                                           LaserBoy_3D_double s
                                                          );
    //------------------------------------------------------------------------
    void                    scale_around_origin           (LaserBoy_3D_double s);
    //------------------------------------------------------------------------
    LaserBoy_3D_double      rectangular_center_of         () const ;
    LaserBoy_3D_double      mean_of_coordinates           () const ;
    LaserBoy_3D_double      centroid_of_coordinates_xy    (double& area, LaserBoy_real_segment& rs) const ;
    LaserBoy_3D_double      centroid_of_coordinates_zy    (double& area, LaserBoy_real_segment& rs) const ;
    LaserBoy_3D_double      centroid_of_coordinates_xz    (double& area, LaserBoy_real_segment& rs) const ;
    LaserBoy_3D_double      centroid_of_coordinates       () const ;
    //------------------------------------------------------------------------
    LaserBoy_real_segment   polygon_outline_xy            (double& area) const ;
    LaserBoy_real_segment   polygon_outline_zy            (double& area) const ;
    LaserBoy_real_segment   polygon_outline_xz            (double& area) const ;
    //------------------------------------------------------------------------
    u_int                   number_of_segments            () const ;
    //------------------------------------------------------------------------
    bool                    find_segment_at_index         (u_int  index,
                                                           u_int& start,
                                                           u_int& end
                                                          )             const ;
    //------------------------------------------------------------------------
    LaserBoy_real_segment&  fracture_segments             ();
    LaserBoy_real_segment&  bond_segments                 ();
    LaserBoy_real_segment&  sort_by_rotation_xy           ();
    LaserBoy_real_segment&  sort_by_rotation_zy           ();
    LaserBoy_real_segment&  sort_by_rotation_xz           ();
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set explode_segments            () const ;
    //------------------------------------------------------------------------
    LaserBoy_real_segment   copy_segment                  (u_int index) const ;
    LaserBoy_3D_double      rectangular_center_of_segment (u_int index) const ;
    LaserBoy_3D_double      mean_of_coordinates_of_segment(u_int index) const ;
    LaserBoy_3D_double      centroid_of_segment_xy        (u_int index, double& area, LaserBoy_real_segment& rs) const ;
    LaserBoy_3D_double      centroid_of_segment_xz        (u_int index, double& area, LaserBoy_real_segment& rs) const ;
    LaserBoy_3D_double      centroid_of_segment_zy        (u_int index, double& area, LaserBoy_real_segment& rs) const ;
    LaserBoy_3D_double      centroid_of_segment           (u_int index) const ;
    void                    move_segment                  (u_int index, LaserBoy_3D_double f);
    void                    rotate_segment                (u_int index, LaserBoy_3D_double a);
    void                    rotate_segment_around_origin  (u_int index, LaserBoy_3D_double a);
    void                    scale_segment                 (u_int index, LaserBoy_3D_double m);
    void                    scale_segment_around_origin   (u_int index, LaserBoy_3D_double m);
    //------------------------------------------------------------------------
    LaserBoy_Error_Code     from_ifstream_dxf             (std::ifstream& in);
    LaserBoy_Error_Code     from_ifstream_txt             (std::ifstream&  in,
                                                           const u_int&    group_type,
                                                           int&            line_number
                                                          );
    //------------------------------------------------------------------------
    void                    normalize                     (bool ignore_origin = true);
    void                    normalize_vectors             (bool ignore_origin = true);
    void                    normalize_vectors_with_origin (bool ignore_origin = true);
    //------------------------------------------------------------------------
    LaserBoy_real_segment&  clip                          (const LaserBoy_3D_double& max,
                                                           const LaserBoy_3D_double& min,
                                                           const double&             granularity
                                                          );
    //------------------------------------------------------------------------
    LaserBoy_real_segment&  clip                          ();
    //------------------------------------------------------------------------
    LaserBoy_real_segment&  clip_around_coordinate        (const LaserBoy_3D_double& center,
                                                           const LaserBoy_3D_double& max,
                                                           const LaserBoy_3D_double& min,
                                                           const double&             granularity
                                                          );
    //------------------------------------------------------------------------
    LaserBoy_real_segment&  clip_around_coordinate        (const LaserBoy_3D_double& center,
                                                           const double              range,
                                                           const double&             granularity
                                                          );
    //------------------------------------------------------------------------
    LaserBoy_space*      p_space           ;
    int                  palette_index     ;
    LaserBoy_Error_Code  real_segment_error;
    //------------------------------------------------------------------------
};

//############################################################################
//////////////////////////////////////////////////////////////////////////////
//############################################################################
class LaserBoy_real_segment_set : public vector<LaserBoy_real_segment>
{
public:
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set(LaserBoy_space* ps)
        : p_space(ps)              {}
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set(LaserBoy_space*   ps,
                              const int         n,
                              const double      radius,
                              const double      pedals_numerator,
                              const double      pedals_denominator,
                              const double      _radius,
                              const double      _pedals_numerator,
                              const double      _pedals_denominator,
                              const double      start,
                              const double      uration,
                              const int         frames
                             );  // uddddddddu animated rhodoneas (uddddd rhodonea)
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set(LaserBoy_space*   ps,
                              const double      center_radius,
                              const double      roller_radius,
                              const double      roller_offset,
                              const double      _center_radius,
                              const double      _roller_radius,
                              const double      _roller_offset,
                              const int         n,
                              const double      start,
                              const double      duration,
                              const int         frames
                             ); // dddddduddu animated epitrochoids epicycloids (dddudd epitrochoid)
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set(LaserBoy_space*   ps,
                              const double      start,
                              const double      duration,
                              const int         frames,
                              const double      center_radius,
                              const double      roller_radius,
                              const double      roller_offset,
                              const double      _center_radius,
                              const double      _roller_radius,
                              const double      _roller_offset,
                              const int         n
                             ); // dduddddddu animated hypotrochoids hypocycloids (dddddu hypotrochoid hypocycloid)
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set(LaserBoy_space*   ps,
                              LaserBoy_pendulum P1,
                              LaserBoy_pendulum _P1,
                              const int         n,
                              const double      duration,
                              const int         frames
                             ); // animated pendulums constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set(LaserBoy_space*   ps,
                              const double      duration,
                              const int         n,
                              LaserBoy_pendulum P1,
                              LaserBoy_pendulum P2,
                              LaserBoy_pendulum _P1,
                              LaserBoy_pendulum _P2,
                              const int         frames
                             ); // animated pendulums_sum constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set(LaserBoy_space*   ps,
                              LaserBoy_pendulum P1,
                              LaserBoy_pendulum P2,
                              LaserBoy_pendulum _P1,
                              LaserBoy_pendulum _P2,
                              const int         n,
                              const double      duration,
                              const int         frames
                             ); // animated pendulums_xy constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set(LaserBoy_space*   ps,
                              LaserBoy_pendulum P1,
                              LaserBoy_pendulum P2,
                              LaserBoy_pendulum P3,
                              LaserBoy_pendulum _P1,
                              LaserBoy_pendulum _P2,
                              LaserBoy_pendulum _P3,
                              const int         n,
                              const double      duration,
                              const int         frames
                             ); // animated pendulums_xy constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set(LaserBoy_space*   ps,
                              LaserBoy_pendulum P1,
                              LaserBoy_pendulum P2,
                              LaserBoy_pendulum P3,
                              LaserBoy_pendulum P4,
                              LaserBoy_pendulum _P1,
                              LaserBoy_pendulum _P2,
                              LaserBoy_pendulum _P3,
                              LaserBoy_pendulum _P4,
                              const int         n,
                              const double      duration,
                              const int         frames
                             ); // animated harmonographs constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set(LaserBoy_space*   ps,
                              LaserBoy_pendulum P1,
                              LaserBoy_pendulum P2,
                              LaserBoy_pendulum P3,
                              LaserBoy_pendulum P4,
                              LaserBoy_pendulum P5,
                              LaserBoy_pendulum P6,
                              LaserBoy_pendulum _P1,
                              LaserBoy_pendulum _P2,
                              LaserBoy_pendulum _P3,
                              LaserBoy_pendulum _P4,
                              LaserBoy_pendulum _P5,
                              LaserBoy_pendulum _P6,
                              const int         n,
                              const double      duration,
                              const int         frames
                             ); // animated harmonographs_3D constructor
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set(LaserBoy_space*   ps,
                              const int         n,
                              const double      duration,
                              LaserBoy_pendulum P1,
                              LaserBoy_pendulum P2,
                              LaserBoy_pendulum _P1,
                              LaserBoy_pendulum _P2,
                              const int         frames
                             ); // udppppu animated amplitude_mods constructor (udpp amplitude_mod)
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set(LaserBoy_space*   ps,
                              const int         n,
                              const double      duration,
                              LaserBoy_pendulum P1,
                              LaserBoy_pendulum P2,
                              LaserBoy_pendulum P3,
                              LaserBoy_pendulum P4,
                              LaserBoy_pendulum _P1,
                              LaserBoy_pendulum _P2,
                              LaserBoy_pendulum _P3,
                              LaserBoy_pendulum _P4,
                              const int         frames
                             ); // udppppppppu animated amplitude_mods_xy constructor (udpppp amplitude_mod_xy)
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set(LaserBoy_space*   ps,
                              const int         n,
                              const double      duration,
                              LaserBoy_pendulum P1,
                              LaserBoy_pendulum P2,
                              LaserBoy_pendulum P3,
                              LaserBoy_pendulum P4,
                              LaserBoy_pendulum P5,
                              LaserBoy_pendulum P6,
                              LaserBoy_pendulum _P1,
                              LaserBoy_pendulum _P2,
                              LaserBoy_pendulum _P3,
                              LaserBoy_pendulum _P4,
                              LaserBoy_pendulum _P5,
                              LaserBoy_pendulum _P6,
                              const int         frames
                             ); // udppppppppppppu animated amplitude_mods_xyz constructor (udpppppp amplitude_mod_xyz)
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set(LaserBoy_space*   ps,
                              const int         n,
                              LaserBoy_pendulum P1,
                              LaserBoy_pendulum P2,
                              LaserBoy_pendulum _P1,
                              LaserBoy_pendulum _P2,
                              const double      duration,
                              const int         frames
                             ); // uppppdu animated frequency_mod constructor (uppd frequency_mod)
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set(LaserBoy_space*   ps,
                              const int         n,
                              LaserBoy_pendulum P1,
                              LaserBoy_pendulum P2,
                              LaserBoy_pendulum P3,
                              LaserBoy_pendulum P4,
                              LaserBoy_pendulum _P1,
                              LaserBoy_pendulum _P2,
                              LaserBoy_pendulum _P3,
                              LaserBoy_pendulum _P4,
                              const double      duration,
                              const int         frames
                             ); // uppppppppdu animated frequency_mod_xy constructor (uppppd frequency_mod_xy)
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set(LaserBoy_space*   ps,
                              const int         n,
                              LaserBoy_pendulum P1,
                              LaserBoy_pendulum P2,
                              LaserBoy_pendulum P3,
                              LaserBoy_pendulum P4,
                              LaserBoy_pendulum P5,
                              LaserBoy_pendulum P6,
                              LaserBoy_pendulum _P1,
                              LaserBoy_pendulum _P2,
                              LaserBoy_pendulum _P3,
                              LaserBoy_pendulum _P4,
                              LaserBoy_pendulum _P5,
                              LaserBoy_pendulum _P6,
                              const double      duration,
                              const int         frames
                             ); // uppppppppppppdu animated frequency_mod_xyz constructor (uppppppd frequency_mod_xyz)
    //------------------------------------------------------------------------
virtual
   ~LaserBoy_real_segment_set()
            { clear(); }
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set& operator += (const LaserBoy_real_segment& rs)
        {
            push_back(rs);
            return *this;
        }
    //------------------------------------------------------------------------
    void reduce_blank_vectors           ()
    {
        for(u_int i = 0; i < size(); i++)
            at(i).reduce_blank_vectors();
    }
    //------------------------------------------------------------------------
    void scale                          (LaserBoy_3D_double s);
    void normalize                      (bool ignore_origin = true);
    void normalize_vectors              (bool ignore_origin = true);
    void normalize_vectors_with_origin  (bool ignore_origin = true);
    //------------------------------------------------------------------------
    LaserBoy_real_segment_set& reverse  ();
    void                       strip_color_rgb(const LaserBoy_color& c);
    void                       sync_rgb_and_palette();
    //------------------------------------------------------------------------
    LaserBoy_real_segment      sum_of_frames()
    {
        LaserBoy_real_segment rs(p_space);
        for(u_int i = 0; i < size(); i++)
            rs += at(i);
        return rs;
    }
    //------------------------------------------------------------------------
    LaserBoy_space*   p_space;
    //------------------------------------------------------------------------
};

//############################################################################
#endif

//############################################################################
//////////////////////////////////////////////////////////////////////////////
//############################################################################
