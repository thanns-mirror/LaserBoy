//############################################################################
//
// LaserBoy !!!
//
// by James Lehman
// Extra Stimulus Inc.
// james@akrobiz.com
//
// began: October 2003
//
// Copyright 2003 to 2020 James Lehman.
// This source is distributed under the terms of the GNU General Public License.
//
// LaserBoy_GUI_base.hpp is part of LaserBoy.
//
// LaserBoy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LaserBoy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LaserBoy. If not, see <http://www.gnu.org/licenses/>.
//
//############################################################################
#ifndef __LASERBOY_GUI_BASE_DEFINITIONS__
#define __LASERBOY_GUI_BASE_DEFINITIONS__

#include "LaserBoy_TUI.hpp"

//############################################################################
class LaserBoy_GUI_base
{
public:
    LaserBoy_GUI_base(int x, int y)
        : running            (false),
          prompt_escaped     (false),
          xres               (x    ),
          yres               (y    ),
          space              (this )
                                       {}
    //------------------------------------------------------------------------
virtual
   ~LaserBoy_GUI_base() {}
    //------------------------------------------------------------------------
virtual void     display_space                              (                     ) = 0;
virtual void     capture_screen                             (                     ) = 0;
    //------------------------------------------------------------------------
virtual void     display_prompt_file_with_auto_complete     (const string& prompt,
                                                             const string& value  ) = 0;
    //------------------------------------------------------------------------
virtual void     display_prompt_dir_with_auto_complete      (const string& prompt ) = 0;
virtual void     display_prompt_f_effect_with_auto_complete (const string& prompt ) = 0;
virtual void     display_prompt_fs_effect_with_auto_complete(const string& prompt ) = 0;
virtual string   display_prompt_and_echo_name               (const string& prompt,
                                                             const u_int max_len  ) = 0;
virtual string   display_prompt_and_echo_string             (const string& prompt ) = 0;
    //------------------------------------------------------------------------
virtual double   display_prompt_and_echo_double             (const string& prompt,
                                                             double  value,
                                                             double  v_max,
                                                             double  v_min        ) = 0;
    //------------------------------------------------------------------------
virtual u_char   display_prompt_and_echo_u_char             (const string& prompt,
                                                             u_char        value  ) = 0;
    //------------------------------------------------------------------------
virtual short    display_prompt_and_echo_short              (const string& prompt,
                                                             short         value  ) = 0;
    //------------------------------------------------------------------------
virtual  int     display_prompt_and_echo_int                (const string& prompt,
                                                             int           value = 0
                                                            )                       = 0;
    //------------------------------------------------------------------------
virtual u_int    display_prompt_and_echo_u_int              (const string& prompt,
                                                             u_int         value ,
                                                             u_int         v_max  ) = 0;
    //------------------------------------------------------------------------
virtual int      display_prompt_and_echo_nibble             (const string& prompt ) = 0;
virtual bool     display_prompt_and_echo_bool               (const string& prompt ) = 0;
    //------------------------------------------------------------------------
virtual bool     report_ild_file_open                       (LaserBoy_frame_set&        frame_set,
                                                             string&                    file_name,
                                                             LaserBoy_ild_header_count& counter
                                                            )                       = 0;
    //------------------------------------------------------------------------
virtual bool     report_ctn_file_open                       (LaserBoy_frame_set& frame_set,
                                                             string& file_name
                                                            )                       = 0;
    //------------------------------------------------------------------------
virtual void     display_error                              (const string& error  ) = 0;
virtual void     display_state                              (const string& state  ) = 0;
virtual void     display_message                            (const string& message) = 0;
virtual void     display_progress                           (      int   countdown) = 0;
virtual void     display_please_wait                        (                     ) = 0;
virtual void     display_ild_file_stats                     (LaserBoy_ild_header_count counter) = 0;
    //------------------------------------------------------------------------
virtual void     wait_4_Esc                                 (                     ) = 0;
virtual void     wait_4_any_key                             (                     ) = 0;
    //------------------------------------------------------------------------
    bool         running,
                 prompt_escaped;
    u_int        xres,
                 yres;
    LaserBoy_TUI space;
};

//############################################################################
#endif

//############################################################################
//////////////////////////////////////////////////////////////////////////////
//############################################################################
