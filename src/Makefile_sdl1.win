# Project: LaserBoy
# by James Lehman.

RM       = del /Q /F
STRIP    = strip -s -v
CPP      = g++.exe

INC      = -I"C:\mingw-w64\mingw32\i686-w64-mingw32\include" \
           -I"C:\boost\include\boost-1_74"

LIB      = -L"C:\mingw-w64\mingw32\i686-w64-mingw32\lib" \
           -L"C:\boost\lib"

LIBS     = -lmingw32 \
           -lws2_32 \
           -lSDLmain \
           -lSDL \
           -static-libstdc++ \
           -static-libgcc \
           -Wl,-Bstatic \
           -lgcc \
           -lstdc++ \
           -lpthread \
           -lboost_filesystem-mgw8-mt-x32-1_74 \
           -L"C:\boost\lib\libboost_filesystem-mgw8-mt-x32-1_74.a" \
           -lboost_system-mgw8-mt-x32-1_74 \
           -L"C:\boost\lib\libboost_system-mgw8-mt-x32-1_74.a" \
           -Wl,-Bdynamic

HEADERS   = LaserBoy_includes.hpp \
            LaserBoy_macros.hpp \
            LaserBoy_common.hpp \
            LaserBoy_utility.hpp \
            LaserBoy_font.hpp \
            LaserBoy_bmp.hpp \
            LaserBoy_3D_short.hpp \
            LaserBoy_color.hpp \
            LaserBoy_vertex.hpp \
            LaserBoy_segment.hpp \
            LaserBoy_3D_double.hpp \
            LaserBoy_real_vertex.hpp \
            LaserBoy_real_segment.hpp \
            LaserBoy_ild_header.hpp \
            LaserBoy_frame.hpp \
            LaserBoy_frame_set.hpp \
            LaserBoy_palette.hpp \
            LaserBoy_palette_set.hpp \
            LaserBoy_wave.hpp \
            LaserBoy_GUI_base.hpp \
            LaserBoy_SDL_GUI.hpp \
            LaserBoy_space.hpp \
            LaserBoy_TUI.hpp

OBJ      = ..\src\LaserBoy.o \
           ..\src\LaserBoy_common.o \
           ..\src\LaserBoy_frame.o \
           ..\src\LaserBoy_frame_effects.o \
           ..\src\LaserBoy_selected_frames_effects.o \
           ..\src\LaserBoy_frame_set.o \
           ..\src\LaserBoy_frame_set_effects.o \
           ..\src\LaserBoy_ild_header.o \
           ..\src\LaserBoy_segment.o \
           ..\src\LaserBoy_palette.o \
           ..\src\LaserBoy_palette_set.o \
           ..\src\LaserBoy_real_segment.o \
           ..\src\LaserBoy_wave.o \
           ..\src\LaserBoy_space.o \
           ..\src\LaserBoy_TUI.o \
           ..\src\LaserBoy_SDL_GUI.o \
           ..\src\LaserBoy_bmp.o \
           ..\src\LaserBoy_font.o

CPPFLAGS = -D__MINGW_LASERBOY__ \
           -D_WIN32_WINNT=0x0501 \
           -D_WIN32_WINDOWS=0x0501 \
           -Dmain=SDL_main \
           $(INC) \
           $(LIB) \
           $(LIBS) \
           -std=c++11 \
           -O3 \
           -msse2 \
           -mfpmath=sse \
           -mwindows \
           -Wcpp \
           -Wall \
           -Wextra \
           -Wno-implicit-fallthrough \
           -Wno-misleading-indentation \
           -Wno-missing-field-initializers \
           -Wno-unused-but-set-variable \
           -frounding-math \
           -fsignaling-nans \
           -fexpensive-optimizations \

BIN       = ..\LaserBoy.exe

all: $(BIN)

strip:
	$(STRIP) $(BIN)

clean:
	$(RM) $(OBJ) $(BIN)

$(BIN): $(OBJ)
	$(CPP) $(OBJ) -o $(BIN) $(LIB) $(LIBS)

..\src\LaserBoy.obj: LaserBoy.cpp $(HEADER)
	$(CPP) -c LaserBoy.cpp -o ..\src\LaserBoy.o $(CPPFLAGS)

..\src\LaserBoy_SDL_GUI.o: LaserBoy_SDL_GUI.cpp $(HEADERS)
	$(CPP) -c LaserBoy_SDL_GUI.cpp -o ..\src\LaserBoy_SDL_GUI.o $(CPPFLAGS)

..\src\LaserBoy_TUI.o: LaserBoy_TUI.cpp $(HEADERS)
	$(CPP) -c LaserBoy_TUI.cpp -o ..\src\LaserBoy_TUI.o $(CPPFLAGS)

..\src\LaserBoy_space.o: LaserBoy_space.cpp  $(HEADERS)
	$(CPP) -c LaserBoy_space.cpp -o ..\src\LaserBoy_space.o $(CPPFLAGS)

..\src\LaserBoy_frame_set_effects.o: LaserBoy_frame_set_effects.cpp $(HEADERS)
	$(CPP) -c LaserBoy_frame_set_effects.cpp -o ..\src\LaserBoy_frame_set_effects.o $(CPPFLAGS)

..\src\LaserBoy_selected_frames_effects.o: LaserBoy_selected_frames_effects.cpp $(HEADERS)
	$(CPP) -c LaserBoy_selected_frames_effects.cpp -o ..\src\LaserBoy_selected_frames_effects.o $(CPPFLAGS)

..\src\LaserBoy_frame_effects.o: LaserBoy_frame_effects.cpp $(HEADERS)
	$(CPP) -c LaserBoy_frame_effects.cpp -o ..\src\LaserBoy_frame_effects.o $(CPPFLAGS)

..\src\LaserBoy_palette_set.o: LaserBoy_palette_set.cpp $(HEADERS)
	$(CPP) -c LaserBoy_palette_set.cpp -o ..\src\LaserBoy_palette_set.o $(CPPFLAGS)

..\src\LaserBoy_palette.o: LaserBoy_palette.cpp $(HEADERS)
	$(CPP) -c LaserBoy_palette.cpp -o ..\src\LaserBoy_palette.o $(CPPFLAGS)

..\src\LaserBoy_frame_set.o: LaserBoy_frame_set.cpp $(HEADERS)
	$(CPP) -c LaserBoy_frame_set.cpp -o ..\src\LaserBoy_frame_set.o $(CPPFLAGS)

..\src\LaserBoy_frame.o: LaserBoy_frame.cpp $(HEADERS)
	$(CPP) -c LaserBoy_frame.cpp -o ..\src\LaserBoy_frame.o $(CPPFLAGS)

..\src\LaserBoy_ild_header.o: LaserBoy_ild_header.cpp LaserBoy_ild_header.hpp LaserBoy_common.hpp LaserBoy_macros.hpp LaserBoy_includes.hpp
	$(CPP) -c LaserBoy_ild_header.cpp -o ..\src\LaserBoy_ild_header.o $(CPPFLAGS)

..\src\LaserBoy_bmp.o: LaserBoy_bmp.cpp LaserBoy_bmp.hpp LaserBoy_font.hpp LaserBoy_color.hpp LaserBoy_common.hpp LaserBoy_macros.hpp LaserBoy_includes.hpp
	$(CPP) -c LaserBoy_bmp.cpp -o ..\src\LaserBoy_bmp.o $(CPPFLAGS)

..\src\LaserBoy_font.o: LaserBoy_font.cpp LaserBoy_font.hpp
	$(CPP) -c LaserBoy_font.cpp -o ..\src\LaserBoy_font.o $(CPPFLAGS)

..\src\LaserBoy_segment.o: LaserBoy_segment.cpp $(HEADERS)
	$(CPP) -c LaserBoy_segment.cpp -o ..\src\LaserBoy_segment.o $(CPPFLAGS)

..\src\LaserBoy_real_segment.o: LaserBoy_real_segment.cpp $(HEADERS)
	$(CPP) -c LaserBoy_real_segment.cpp -o ..\src\LaserBoy_real_segment.o $(CPPFLAGS)

..\src\LaserBoy_wave.o: LaserBoy_wave.cpp LaserBoy_wave.hpp LaserBoy_common.hpp LaserBoy_macros.hpp LaserBoy_includes.hpp
	$(CPP) -c LaserBoy_wave.cpp -o ..\src\LaserBoy_wave.o $(CPPFLAGS)

..\src\LaserBoy_common.o: LaserBoy_common.cpp LaserBoy_common.hpp LaserBoy_macros.hpp LaserBoy_includes.hpp
	$(CPP) -c LaserBoy_common.cpp -o ..\src\LaserBoy_common.o $(CPPFLAGS)

