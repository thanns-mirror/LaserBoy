//############################################################################
//
// LaserBoy !!!
//
// by James Lehman
// Extra Stimulus Inc.
// james@akrobiz.com
//
// began: October 2003
//
// Copyright 2003 to 2020 James Lehman.
// This source is distributed under the terms of the GNU General Public License.
//
// LaserBoy_real_segment.cpp is part of LaserBoy.
//
// LaserBoy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LaserBoy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LaserBoy. If not, see <http://www.gnu.org/licenses/>.
//
//############################################################################
#include "LaserBoy_GUI_base.hpp"

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space* ps,
                                             LaserBoy_real_vertex from,
                                             LaserBoy_real_vertex to
                                            ) // 3D line function
                : p_space            (ps),
                  palette_index      (LASERBOY_ILDA_DEFAULT),
                  real_segment_error (LASERBOY_OK)
{   // 3D line constructor
    int steps = linear_steps(to, from, (   (to.is_lit())
                                         ? (p_space->lit_delta_max)
                                         : (p_space->blank_delta_max)
                                       )
                            );
    if(steps)
    {
        LaserBoy_3D_double _to        (to),
                           _from      (from),
                           difference (_to - _from),
                           delta      (difference / steps);
        //----------------------------------------------------------------
        for(int i = 1; i < steps; i++)
            push_back(LaserBoy_real_vertex( LaserBoy_3D_double(_from + (delta * i)),
                                            (LaserBoy_color)to,
                                            to.k,
                                            to.c
                                          )
                     );
        //----------------------------------------------------------------
    }
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*      ps,
                                             LaserBoy_real_vertex from,
                                             LaserBoy_real_vertex to,
                                             const u_int&         max_d
                                            ) // 3D line function
                : p_space            (ps),
                  palette_index      (LASERBOY_ILDA_DEFAULT),
                  real_segment_error (LASERBOY_OK)
{   // 3D line constructor
    int steps = linear_steps(to, from, max_d);
    if(steps)
    {
        LaserBoy_3D_double _to        (to),
                           _from      (from),
                           difference (_to - _from),
                           delta      (difference / steps);
        //----------------------------------------------------------------
        for(int i = 1; i < steps; i++)
            push_back(LaserBoy_real_vertex( LaserBoy_3D_double(_from + (delta * i)),
                                            (LaserBoy_color)to,
                                            to.k,
                                            to.c
                                          )
                     );
        //----------------------------------------------------------------
    }
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*           ps,
                                             const LaserBoy_3D_double& p,
                                             const LaserBoy_color      c
                                            ) // 3D cross function
                : p_space            (ps),
                  palette_index      (LASERBOY_TRUE_COLOR),
                  real_segment_error (LASERBOY_OK)
{
    push_back(LaserBoy_real_vertex(     p.x,      p.y,      p.z, c.r, c.g, c.b, LASERBOY_BLANKING_BIT, 0));
    push_back(LaserBoy_real_vertex(     p.x,      p.y,      p.z, c.r, c.g, c.b,                     0, 0));
    push_back(LaserBoy_real_vertex(-32767.0,      p.y,      p.z, c.r, c.g, c.b, LASERBOY_BLANKING_BIT, 0));
    push_back(LaserBoy_real_vertex( 32767.0,      p.y,      p.z, c.r, c.g, c.b,                     0, 0));
    push_back(LaserBoy_real_vertex(     p.x, -32767.0,      p.z, c.r, c.g, c.b, LASERBOY_BLANKING_BIT, 0));
    push_back(LaserBoy_real_vertex(     p.x,  32767.0,      p.z, c.r, c.g, c.b,                     0, 0));
    push_back(LaserBoy_real_vertex(     p.x,      p.y, -32767.0, c.r, c.g, c.b, LASERBOY_BLANKING_BIT, 0));
    push_back(LaserBoy_real_vertex(     p.x,      p.y,  32767.0, c.r, c.g, c.b,                     0, 0));
    sync_rgb_and_palette();
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*     ps,
                                             LaserBoy_frame_set& font_frames,
                                             const string&       text,
                                             const double        mono_font_space,
                                             const bool          bond_word,
                                             const bool          bond_line
                                            )
                : p_space            (ps)
                , palette_index      (LASERBOY_ILDA_DEFAULT)
                , real_segment_error (LASERBOY_OK)
{   // mono spaced font constructor
    if(text.size() == 0)
        real_segment_error = LASERBOY_EMPTY_STRING_ERROR;
    else if(font_frames.number_of_frames() < ('~' - '!' + 1)) // are there enough frames?
        real_segment_error = LASERBOY_BAD_FONT_FILE;
    else
    {
        bool                  first_glyph_rendered = false;
        int                   text_index,
                              vertex_index;
        LaserBoy_3D_double    float_3D;
        LaserBoy_real_vertex  vertex;
        vertex.blank();
        vertex.c = 0;
        vertex.r = 255;
        vertex.g = 0;
        vertex.b = 0;
        float_3D = (LaserBoy_3D_double)font_frames[text[0] - '!'].at(0);
        vertex = float_3D;
        push_back(vertex);
        vertex.unblank();
        for(text_index = 0; text_index < (int)text.size(); text_index++)
        {
            if(text[text_index] >= '!' && text[text_index] <= '~')
            {
                for(vertex_index = 0; vertex_index < (int)font_frames[text[text_index] - '!'].size(); vertex_index++)
                {
                    float_3D = font_frames[text[text_index] - '!'].at(vertex_index);
                    //--------------------------------------------------------
                    float_3D.x = float_3D.x + (   text_index
                                                * (   mono_font_space
                                                    * LASERBOY_MAX_SHORT
                                                  )
                                              );
                    //--------------------------------------------------------
                    if(vertex_index && font_frames[text[text_index] - '!'].at(vertex_index).is_black(p_space->black_level))
                        push_back(LaserBoy_real_vertex(float_3D,
                                                       LaserBoy_color(0, 0, 0),
                                                       vertex.k,
                                                       255
                                                      )
                                 );
                    else
                        push_back(LaserBoy_real_vertex(float_3D,
                                                       LaserBoy_color(255, 255, 255),
                                                       vertex.k,
                                                       55
                                                      )
                                 );
                    if(font_frames[text[text_index] - '!'].at(vertex_index).is_blank())
                    {
                        if(text_index && (bond_word || bond_line)) // not the first glyph
                        {
                            back().r = 0;
                            back().g = 0;
                            back().b = 0;
                            back().c = 255;
                        }
                        else
                            back().blank();
                    }
                }
                first_glyph_rendered = true;
            } // end if(text[text_index] >= '!' && text[text_index] <= '~')
            else
            {
                while(    (text_index + 1) < (int)text.size()
                       && text[text_index + 1] == ' '
                     )
                    text_index++;
                if((text_index + 1) < ((int)text.size() - 1))
                {
                    float_3D = font_frames[text[text_index + 1] - '!'].at(0);
                    //--------------------------------------------------------
                    float_3D.x = float_3D.x + (   (   text_index
                                                    + 1
                                                  )
                                                * (   mono_font_space
                                                    * LASERBOY_MAX_SHORT
                                                  )
                                              );
                    //--------------------------------------------------------
                    if(first_glyph_rendered && bond_line)
                        push_back(LaserBoy_real_vertex(float_3D,
                                                       LaserBoy_color(0, 0, 0),
                                                       vertex.k,
                                                       255
                                                      )
                                 );
                    else
                    {
                        push_back(LaserBoy_real_vertex(float_3D,
                                                       LaserBoy_color(255, 255, 255),
                                                       vertex.k,
                                                       55
                                                      )
                                 );
                        back().blank();
                    }
                }
            }
        } // end for(text_index = 0; text_index < (int)text.size(); text_index++)
        //*this = sub(begin() + 1, end());
        reduce_blank_vectors();
    }
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*     ps,
                                             LaserBoy_frame_set& font_frames,
                                             const string&       text,
                                             const double        vari_font_gap,
                                             const double        vari_font_space,
                                             const bool          bond_word,
                                             const bool          bond_line
                                            )
                : p_space            (ps)
                , palette_index      (LASERBOY_ILDA_DEFAULT)
                , real_segment_error (LASERBOY_OK)
{   // variable spaced font constructor
    if(text.size() == 0)
        real_segment_error = LASERBOY_EMPTY_STRING_ERROR;
    else if(font_frames.number_of_frames() < ('~' - '!' + 1)) // are there enough frames?
        real_segment_error = LASERBOY_BAD_FONT_FILE;
    else
    {
        bool                  first_glyph_rendered = false;
        int                   text_index,
                              vertex_index;

        double                glyph_offset,
                              width_of_a_space   = 0.0,
                              accumulated_offset = 0.0;
        LaserBoy_3D_double    float_3D;
        LaserBoy_real_vertex  vertex;
        vertex.blank();
        vertex.c = 0;
        vertex.r = 255;
        vertex.g = 0;
        vertex.b = 0;
        float_3D = (LaserBoy_3D_double)font_frames[text[0] - '!'].at(0);
        vertex = float_3D;
        push_back(vertex);
        vertex.unblank();
        //----------------------------------------------------------------
        if(vari_font_space < 0.0)
        {
            for(u_int i = 0; i < font_frames.size(); i++)
                if(width_of_a_space < font_frames[i].width())
                    width_of_a_space = font_frames[i].width();
        }
        else                
            width_of_a_space = (LASERBOY_MAX_SHORT * vari_font_space);
        //----------------------------------------------------------------
        for(text_index = 0; text_index < (int)text.size(); text_index++)
        {
            if(text[text_index] >= '!' && text[text_index] <= '~')
            {
                glyph_offset = font_frames[text[text_index] - '!'].segment_left().x;
                for(vertex_index = 0; vertex_index < (int)font_frames[text[text_index] - '!'].size(); vertex_index++)
                {
                    float_3D   = font_frames[text[text_index] - '!'].at(vertex_index);
                    float_3D.x = (float_3D.x - glyph_offset) + accumulated_offset;
                    if(font_frames[text[text_index] - '!'].at(vertex_index).is_black(p_space->black_level))
                        push_back(LaserBoy_real_vertex(float_3D,
                                                       LaserBoy_color(0, 0, 0),
                                                       vertex.k,
                                                       255
                                                      )
                                 );
                    else
                        push_back(LaserBoy_real_vertex(float_3D,
                                                       LaserBoy_color(255, 255, 255),
                                                       vertex.k,
                                                       55
                                                      )
                                 );
                    if(font_frames[text[text_index] - '!'].at(vertex_index).is_blank())
                    {
                        if(text_index && (bond_word || bond_line)) // not the first glyph
                        {
                            back().r = 0;
                            back().g = 0;
                            back().b = 0;
                            back().c = 255;
                        }
                        else
                            back().blank();
                    }
                } // end for(vertex_index = 0; vertex_index < (int)font_frames[text[text_index] - '!'].size(); vertex_index++)
                accumulated_offset += (   font_frames[text[text_index] - '!'].width()
                                        + (   vari_font_gap
                                            * LASERBOY_MAX_SHORT // percent of square space
                                          )
                                      );
                first_glyph_rendered = true;
            } // end if(text[text_index] >= '!' && text[text_index] <= '~')
            else // space character
            {
                accumulated_offset += (   width_of_a_space
                                        + (   vari_font_gap
                                            * LASERBOY_MAX_SHORT // percent of square space
                                          )
                                      );
                while(    (text_index + 1) < (int)text.size()
                       && text[text_index + 1] == ' '
                     )
                {
                    accumulated_offset += (   width_of_a_space
                                            + (   vari_font_gap
                                                * LASERBOY_MAX_SHORT // percent of square space
                                              )
                                          );
                    text_index++;
                }
                if((text_index + 1) < ((int)text.size() - 1))
                {
                    glyph_offset = font_frames[text[text_index + 1] - '!'].segment_left().x;
                    float_3D = font_frames[text[text_index + 1] - '!'].at(0);
                    float_3D.x = (float_3D.x - glyph_offset) + accumulated_offset;                    
                    if(first_glyph_rendered && bond_line)
                        push_back(LaserBoy_real_vertex(float_3D,
                                                       LaserBoy_color(0, 0, 0),
                                                       vertex.k,
                                                       255
                                                      )
                                 );
                    else
                    {
                        push_back(LaserBoy_real_vertex(float_3D,
                                                       LaserBoy_color(255, 255, 255),
                                                       vertex.k,
                                                       55
                                                      )
                                 );
                        back().blank();
                    }
                }
            }
        } // end for(text_index = 0; text_index < (int)text.size(); text_index++)
        reduce_blank_vectors();
    }
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*   ps,
                                             const int         n,
                                             const double      radius,
                                             const double      pedals_numerator,
                                             const double      pedals_denominator,
                                             const double      start,
                                             const double      duration
                                            ) // uddddd rhodonea
                : p_space            (ps),
                  palette_index      (LASERBOY_ILDA_DEFAULT),
                  real_segment_error (LASERBOY_OK)                  
{
    double time = 0.0,
           ratio = 1.0;
    if(pedals_denominator != 0.0)
           ratio = pedals_numerator / pedals_denominator;
    LaserBoy_real_vertex vertex;
    vertex.blank();
    vertex.c = p_space->selected_color_index;
    vertex.r = p_space->palette_picker(p_space->palette_index)[vertex.c].r;
    vertex.g = p_space->palette_picker(p_space->palette_index)[vertex.c].g;
    vertex.b = p_space->palette_picker(p_space->palette_index)[vertex.c].b;
    vertex.x = radius * cos(start) * cos(start);
    vertex.y = radius * cos(start) * sin(start);
    vertex.z = 0.0;
    push_back(vertex);
    push_back(vertex);
    push_back(vertex);
    vertex.unblank();
    p_space->p_GUI->display_state("rendering rhodonea");
    for(int i = 0; i <= n; i++)
    {
        time = i * duration / n;
        vertex.x = radius * cos(ratio * time + start) * cos(time + start);
        vertex.y = radius * cos(ratio * time + start) * sin(time + start);
        push_back(vertex);
    }
    sync_rgb_and_palette();
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*   ps,
                                             const double      center_radius,
                                             const double      roller_radius,
                                             const double      roller_offset,
                                             const int         n,
                                             const double      start,
                                             const double      duration
                                            ) // dddudd epitrochoid epicycloid
                : p_space            (ps),
                  palette_index      (LASERBOY_ILDA_DEFAULT),
                  real_segment_error (LASERBOY_OK)
{
    double time = 0.0,
           ratio = 1.0;
    if(roller_radius != 0.0)
        ratio = center_radius / roller_radius;
    LaserBoy_real_vertex vertex;
    vertex.blank();
    vertex.c = p_space->selected_color_index;
    vertex.r = p_space->palette_picker(p_space->palette_index)[vertex.c].r;
    vertex.g = p_space->palette_picker(p_space->palette_index)[vertex.c].g;
    vertex.b = p_space->palette_picker(p_space->palette_index)[vertex.c].b;
    vertex.x =   roller_radius * (ratio + 1) * cos(start)
               - roller_offset * cos(start);
    vertex.y =   roller_radius * (ratio + 1) * sin(start)
               - roller_offset * sin(start);
    vertex.z = 0.0;
    push_back(vertex);
    push_back(vertex);
    push_back(vertex);
    vertex.unblank();
    p_space->p_GUI->display_state("rendering epitrochoid / epicycloid");
    for(int i = 0; i <= n; i++)
    {
        time = i * duration / n;
        vertex.x =   roller_radius * (ratio + 1) * cos(time + start)
                   - roller_offset * cos((ratio + 1) * time + start);
        vertex.y =   roller_radius * (ratio + 1) * sin(time + start)
                   - roller_offset * sin((ratio + 1) * time + start);
        push_back(vertex);
    }
    sync_rgb_and_palette();
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*   ps,
                                             const double      start,
                                             const double      duration,
                                             const double      center_radius,
                                             const double      roller_radius,
                                             const double      roller_offset,
                                             const int         n
                                            ) // dddddu hypotrochoid hypocycloid
                : p_space            (ps),
                  palette_index      (LASERBOY_ILDA_DEFAULT),
                  real_segment_error (LASERBOY_OK)
{
    double time = 0.0,
           ratio = 1.0;
    if(roller_radius != 0.0)
        ratio = center_radius / roller_radius;
    LaserBoy_real_vertex vertex;
    vertex.blank();
    vertex.c = p_space->selected_color_index;
    vertex.r = p_space->palette_picker(p_space->palette_index)[vertex.c].r;
    vertex.g = p_space->palette_picker(p_space->palette_index)[vertex.c].g;
    vertex.b = p_space->palette_picker(p_space->palette_index)[vertex.c].b;
    vertex.x =    (center_radius - roller_radius) * cos(start)
                + roller_offset * cos(start);
    vertex.y =    (center_radius - roller_radius) * sin(start)
                - roller_offset * sin(start);
    vertex.z = 0.0;
    push_back(vertex);
    push_back(vertex);
    push_back(vertex);
    vertex.unblank();
    p_space->p_GUI->display_state("rendering hypotrochoid / hypocycloid");
    for(int i = 0; i <= n; i++)
    {
        time = i * duration / n;
        vertex.x =    (center_radius - roller_radius) * cos(time + start)
                    + roller_offset * cos((ratio - 1) * time + start);
        vertex.y =    (center_radius - roller_radius) * sin(time + start)
                    - roller_offset * sin((ratio - 1) * time + start);
        push_back(vertex);
    }
    sync_rgb_and_palette();
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*   ps,
                                             LaserBoy_pendulum P1,
                                             const int         n,
                                             const double      duration
                                            ) // pendulum constructor
                : p_space            (ps),
                  real_segment_error (LASERBOY_OK)                         
{
    double                time;
    LaserBoy_real_vertex  vertex;
    palette_index = p_space->current_frame().palette_index;
    vertex.c = p_space->selected_color_index;
    vertex.r = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].r;
    vertex.g = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].g;
    vertex.b = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].b;
    vertex.blank();
    vertex.x = 0.0;
    vertex.y = P1.position(0.0);
    push_back(vertex);
    push_back(vertex);
    push_back(vertex);
    vertex.unblank();
    p_space->p_GUI->display_state("rendering pendulum");
    for(int i = 0; i <= n; i++)
    {
        time = i * duration / n ;
        vertex.x = time;
        vertex.y = P1.position(time);
        push_back(vertex);
    }
    sync_rgb_and_palette();
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*   ps,
                                             const double      duration,
                                             const int         n,
                                             LaserBoy_pendulum P1,
                                             LaserBoy_pendulum P2
                                            ) // pendulum_sum constructor
                : p_space            (ps),
                  real_segment_error (LASERBOY_OK)
{
    double                time;
    LaserBoy_real_vertex  vertex;
    palette_index = p_space->current_frame().palette_index;
    vertex.c = p_space->selected_color_index;
    vertex.r = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].r;
    vertex.g = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].g;
    vertex.b = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].b;
    vertex.blank();
    vertex.x = 0.0;
    vertex.y = P1.position(0.0) + P2.position(0.0);
    push_back(vertex);
    push_back(vertex);
    push_back(vertex);
    vertex.unblank();
    p_space->p_GUI->display_state("rendering pendulum_sum");
    for(int i = 0; i <= n; i++)
    {
        time = i * duration / n ;
        vertex.x = time;
        vertex.y = P1.position(time) + P2.position(time);
        push_back(vertex);
    }
    sync_rgb_and_palette();
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*   ps,
                                             LaserBoy_pendulum P1,
                                             LaserBoy_pendulum P2,
                                             const int         n,
                                             const double      duration
                                            ) // pendulum_xy constructor
                : p_space            (ps),
                  real_segment_error (LASERBOY_OK)
{
    double                time;
    LaserBoy_real_vertex  vertex;
    palette_index = p_space->current_frame().palette_index;
    vertex.c = p_space->selected_color_index;
    vertex.r = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].r;
    vertex.g = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].g;
    vertex.b = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].b;
    vertex.blank();
    vertex.x = P1.position(0.0);
    vertex.y = P2.position(0.0);
    push_back(vertex);
    push_back(vertex);
    push_back(vertex);
    vertex.unblank();
    p_space->p_GUI->display_state("rendering pendulum_xy");
    for(int i = 0; i <= n; i++)
    {
        time = i * duration / n ;
        vertex.x = P1.position(time);
        vertex.y = P2.position(time);
        push_back(vertex);
    }
    sync_rgb_and_palette();
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*   ps,
                                             LaserBoy_pendulum P1,
                                             LaserBoy_pendulum P2,
                                             LaserBoy_pendulum P3,
                                             const int         n,
                                             const double      duration
                                            ) // pendulum_xyz constructor
                : p_space            (ps),
                  real_segment_error (LASERBOY_OK)
{
    double                time;
    LaserBoy_real_vertex  vertex;
    palette_index = p_space->current_frame().palette_index;
    vertex.c = p_space->selected_color_index;
    vertex.r = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].r;
    vertex.g = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].g;
    vertex.b = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].b;
    vertex.blank();
    vertex.x = P1.position(0.0);
    vertex.y = P2.position(0.0);
    vertex.z = P3.position(0.0);
    push_back(vertex);
    push_back(vertex);
    push_back(vertex);
    vertex.unblank();
    p_space->p_GUI->display_state("rendering pendulum_xyz");
    for(int i = 0; i <= n; i++)
    {
        time = i * duration / n ;
        vertex.x = P1.position(time);
        vertex.y = P2.position(time);
        vertex.z = P3.position(time);
        push_back(vertex);
    }
    sync_rgb_and_palette();
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*   ps,
                                             LaserBoy_pendulum P1,
                                             LaserBoy_pendulum P2,
                                             LaserBoy_pendulum P3,
                                             LaserBoy_pendulum P4,
                                             const int         n,
                                             const double      duration
                                            ) // ppppud harmonograph constructor
                : p_space            (ps),
                  real_segment_error (LASERBOY_OK)                         
{
    double                time;
    LaserBoy_real_vertex  vertex;
    palette_index = p_space->current_frame().palette_index;
    vertex.c = p_space->selected_color_index;
    vertex.r = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].r;
    vertex.g = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].g;
    vertex.b = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].b;
    vertex.blank();
    vertex.x = P1.position(0.0) + P2.position(0.0);
    vertex.y = P3.position(0.0) + P4.position(0.0);
    push_back(vertex);
    push_back(vertex);
    push_back(vertex);
    vertex.unblank();
    p_space->p_GUI->display_state("rendering harmonograph");
    for(int i = 0; i <= n; i++)
    {
        time = i * duration / n ;
        vertex.x = P1.position(time) + P2.position(time);
        vertex.y = P3.position(time) + P4.position(time);
        push_back(vertex);
    }
    sync_rgb_and_palette();
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*   ps,
                                             LaserBoy_pendulum P1,
                                             LaserBoy_pendulum P2,
                                             LaserBoy_pendulum P3,
                                             LaserBoy_pendulum P4,
                                             LaserBoy_pendulum P5,
                                             LaserBoy_pendulum P6,
                                             const int         n,
                                             const double      duration
                                            ) // ppppppud harmonograph_3D constructor
                : p_space            (ps),
                  real_segment_error (LASERBOY_OK)                         
{
    double                time;
    LaserBoy_real_vertex  vertex;
    palette_index = p_space->current_frame().palette_index;
    vertex.c = p_space->selected_color_index;
    vertex.r = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].r;
    vertex.g = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].g;
    vertex.b = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].b;
    vertex.blank();
    vertex.x = P1.position(0.0) + P2.position(0.0);
    vertex.y = P3.position(0.0) + P4.position(0.0);
    vertex.z = P5.position(0.0) + P6.position(0.0);
    push_back(vertex);
    push_back(vertex);
    push_back(vertex);
    vertex.unblank();
    p_space->p_GUI->display_state("rendering harmonograph");
    for(int i = 0; i <= n; i++)
    {
        time = i * duration / n ;
        vertex.x = P1.position(time) + P2.position(time);
        vertex.y = P3.position(time) + P4.position(time);
        vertex.z = P5.position(time) + P6.position(time);
        push_back(vertex);
    }
    sync_rgb_and_palette();
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*   ps,
                                             const int         n,
                                             const double      duration,
                                             LaserBoy_pendulum P1,
                                             LaserBoy_pendulum P2
                                            ) // udpp amplitude_mod constructor
                : p_space            (ps),
                  real_segment_error (LASERBOY_OK)
{
    double                time;
    LaserBoy_real_vertex  vertex;
    palette_index = p_space->current_frame().palette_index;
    vertex.c = p_space->selected_color_index;
    vertex.r = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].r;
    vertex.g = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].g;
    vertex.b = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].b;
    vertex.blank();
    vertex.x = 0.0;
    vertex.y = P1.position(0.0) * P2.position(0.0);
    vertex.z = 0.0;
    push_back(vertex);
    push_back(vertex);
    push_back(vertex);
    vertex.unblank();
    p_space->p_GUI->display_state("rendering amplitude_mod");
    for(int i = 0; i <= n; i++)
    {
        time = i * duration / n ;
        vertex.x = time;
        vertex.y = P1.position(time) * P2.position(time);
        push_back(vertex);
    }
    sync_rgb_and_palette();
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*   ps,
                                             const int         n,
                                             const double      duration,
                                             LaserBoy_pendulum P1,
                                             LaserBoy_pendulum P2,
                                             LaserBoy_pendulum P3,
                                             LaserBoy_pendulum P4
                                            ) // udpppp amplitude_mod_xy constructor
                : p_space            (ps),
                  real_segment_error (LASERBOY_OK)
{
    double                time;
    LaserBoy_real_vertex  vertex;
    palette_index = p_space->current_frame().palette_index;
    vertex.c = p_space->selected_color_index;
    vertex.r = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].r;
    vertex.g = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].g;
    vertex.b = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].b;
    vertex.blank();
    vertex.x = P1.position(0.0) * P2.position(0.0);
    vertex.y = P3.position(0.0) * P4.position(0.0);
    vertex.z = 0.0;
    push_back(vertex);
    push_back(vertex);
    push_back(vertex);
    vertex.unblank();
    p_space->p_GUI->display_state("rendering amplitude_mod_xy");
    for(int i = 0; i <= n; i++)
    {
        time = i * duration / n ;
        vertex.x = P1.position(time) * P2.position(time);
        vertex.y = P3.position(time) * P4.position(time);
        push_back(vertex);
    }
    sync_rgb_and_palette();
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*   ps,
                                             const int         n,
                                             const double      duration,
                                             LaserBoy_pendulum P1,
                                             LaserBoy_pendulum P2,
                                             LaserBoy_pendulum P3,
                                             LaserBoy_pendulum P4,
                                             LaserBoy_pendulum P5,
                                             LaserBoy_pendulum P6
                                            ) // udpppppp amplitude_mod_xyz constructor
                : p_space            (ps),
                  real_segment_error (LASERBOY_OK)
{
    double                time = 0.0;
    LaserBoy_real_vertex  vertex;
    palette_index = p_space->current_frame().palette_index;
    vertex.c = p_space->selected_color_index;
    vertex.r = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].r;
    vertex.g = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].g;
    vertex.b = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].b;
    vertex.blank();
    vertex.x = P1.position(time) * P2.position(time);
    vertex.y = P3.position(time) * P4.position(time);
    vertex.z = P5.position(time) * P6.position(time);
    push_back(vertex);
    push_back(vertex);
    push_back(vertex);
    vertex.unblank();
    p_space->p_GUI->display_state("rendering amplitude_mod_xyz");
    for(int i = 0; i <= n; i++)
    {
        time = i * duration / n ;
        vertex.x = P1.position(time) * P2.position(time);
        vertex.y = P3.position(time) * P4.position(time);
        vertex.z = P5.position(time) * P6.position(time);
        push_back(vertex);
    }
    sync_rgb_and_palette();
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*   ps,
                                             const int         n,
                                             LaserBoy_pendulum P1,
                                             LaserBoy_pendulum P2,
                                             const double      duration
                                            ) // uppd frequency_mod constructor
                : p_space            (ps),
                  real_segment_error (LASERBOY_OK)
{
    double                time = 0.0,
                          p1;
    LaserBoy_real_vertex  vertex;
    palette_index = p_space->current_frame().palette_index;
    vertex.c = p_space->selected_color_index;
    vertex.r = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].r;
    vertex.g = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].g;
    vertex.b = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].b;
    vertex.blank();
    p1 = P1.phase * (two_pi / P1.one_rotation);
    vertex.x = 0.0;
    vertex.y = P1.amplitude * sin(time * P1.frequency * P2.position(time) + p1) * pow(_e_, -P1.damping * time) + P1.offset;
    vertex.z = 0.0;
    push_back(vertex);
    push_back(vertex);
    push_back(vertex);
    vertex.unblank();
    p_space->p_GUI->display_state("rendering frequency_mod");
    for(int i = 0; i <= n; i++)
    {
        time = i * duration / n ;
        p1 = P1.phase * (two_pi / P1.one_rotation);
        vertex.x = time;
        vertex.y = P1.amplitude * sin(time * P1.frequency * P2.position(time) + p1) * pow(_e_, -P1.damping * time) + P1.offset;
        push_back(vertex);
    }
    sync_rgb_and_palette();
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*   ps,
                                             const int         n,
                                             LaserBoy_pendulum P1,
                                             LaserBoy_pendulum P2,
                                             LaserBoy_pendulum P3,
                                             LaserBoy_pendulum P4,
                                             const double      duration
                                            ) // uppppd frequency_mod_xy constructor
                : p_space            (ps),
                  real_segment_error (LASERBOY_OK)
{
    double                time = 0.0,
                          p1, p3;
    LaserBoy_real_vertex  vertex;
    palette_index = p_space->current_frame().palette_index;
    vertex.c = p_space->selected_color_index;
    vertex.r = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].r;
    vertex.g = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].g;
    vertex.b = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].b;
    vertex.blank();
    p1 = P1.phase * (two_pi / P1.one_rotation);
    p3 = P3.phase * (two_pi / P3.one_rotation);
    vertex.x = P1.amplitude * sin(time * P1.frequency * P2.position(time) + p1) * pow(_e_, -P1.damping * time) + P1.offset;
    vertex.y = P3.amplitude * sin(time * P3.frequency * P4.position(time) + p3) * pow(_e_, -P3.damping * time) + P3.offset;
    vertex.z = 0.0;
    push_back(vertex);
    push_back(vertex);
    push_back(vertex);
    vertex.unblank();
    p_space->p_GUI->display_state("rendering frequency_mod_xy");
    for(int i = 0; i <= n; i++)
    {
        time = i * duration / n ;
        p1 = P1.phase * (two_pi / P1.one_rotation);
        p3 = P3.phase * (two_pi / P3.one_rotation);
        vertex.x = P1.amplitude * sin(time * P1.frequency * P2.position(time) + p1) * pow(_e_, -P1.damping * time) + P1.offset;
        vertex.y = P3.amplitude * sin(time * P3.frequency * P4.position(time) + p3) * pow(_e_, -P3.damping * time) + P3.offset;
        push_back(vertex);
    }
    sync_rgb_and_palette();
}

//############################################################################
LaserBoy_real_segment::LaserBoy_real_segment(LaserBoy_space*   ps,
                                             const int         n,
                                             LaserBoy_pendulum P1,
                                             LaserBoy_pendulum P2,
                                             LaserBoy_pendulum P3,
                                             LaserBoy_pendulum P4,
                                             LaserBoy_pendulum P5,
                                             LaserBoy_pendulum P6,
                                             const double      duration
                                            ) // uppppppd frequency_mod_xyz constructor
                : p_space            (ps),
                  real_segment_error (LASERBOY_OK)
{
    double                time = 0.0,
                          p1, p3, p5;
    LaserBoy_real_vertex  vertex;
    palette_index = p_space->current_frame().palette_index;
    vertex.c = p_space->selected_color_index;
    vertex.r = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].r;
    vertex.g = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].g;
    vertex.b = p_space->palette_picker(p_space->current_frame().palette_index)[vertex.c].b;
    vertex.blank();
    p1 = P1.phase * (two_pi / P1.one_rotation);
    p3 = P3.phase * (two_pi / P3.one_rotation);
    p5 = P5.phase * (two_pi / P5.one_rotation);
    vertex.x = P1.amplitude * sin(time * P1.frequency * P2.position(time) + p1) * pow(_e_, -P1.damping * time) + P1.offset;
    vertex.y = P3.amplitude * sin(time * P3.frequency * P4.position(time) + p3) * pow(_e_, -P3.damping * time) + P2.offset;
    vertex.z = P5.amplitude * sin(time * P5.frequency * P6.position(time) + p5) * pow(_e_, -P5.damping * time) + P3.offset;
    push_back(vertex);
    push_back(vertex);
    push_back(vertex);
    vertex.unblank();
    p_space->p_GUI->display_state("rendering frequency_mod_xyz");
    for(int i = 0; i <= n; i++)
    {
        time = i * duration / n ;
        p1 = P1.phase * (two_pi / P1.one_rotation);
        p3 = P3.phase * (two_pi / P3.one_rotation);
        p5 = P5.phase * (two_pi / P5.one_rotation);
        vertex.x = P1.amplitude * sin(time * P1.frequency * P2.position(time) + p1) * pow(_e_, -P1.damping * time) + P1.offset;
        vertex.y = P3.amplitude * sin(time * P3.frequency * P4.position(time) + p3) * pow(_e_, -P3.damping * time) + P2.offset;
        vertex.z = P5.amplitude * sin(time * P5.frequency * P6.position(time) + p5) * pow(_e_, -P5.damping * time) + P3.offset;
        push_back(vertex);
    }
    sync_rgb_and_palette();
}

//############################################################################
u_int LaserBoy_real_segment::number_of_color_vectors() const
{
    u_int count = 0;
    if(size() > 1)
        for(u_int i = 1; i < size(); i++)
            if(at(i).is_color(p_space->black_level))
                count++;
    return count;
}

//############################################################################
u_int LaserBoy_real_segment::number_of_dark_vectors() const
{
    u_int count = 0;
    if(size() > 1)
        for(u_int i = 1; i < size(); i++)
            if(at(i).is_dark(p_space->black_level))
                count++;
    return count;
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::segment_front() const
{
    if(size() > 1)
    {
        LaserBoy_3D_double front_;
        front_.z = -DBL_MAX;
        for(u_int i = 0; i < size(); i++)
            if(at(i).z > front_.z)
                front_ = at(i);
        return front_;
    }
    else
        return LaserBoy_3D_double(0, 0, 0);
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::segment_back() const
{
    if(size() > 1)
    {
        LaserBoy_3D_double back_;
        back_.z = DBL_MAX;
        for(u_int i = 0; i < size(); i++)
            if(at(i).z < back_.z)
                back_ = at(i);
        return back_;
    }
    else
        return LaserBoy_3D_double(0, 0, 0);
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::segment_top() const
{
    if(size() > 1)
    {
        LaserBoy_3D_double top_;
        top_.y = -DBL_MAX;
        for(u_int i = 0; i < size(); i++)
            if(at(i).y > top_.y)
                top_ = at(i);
        return top_;
    }
    else
        return LaserBoy_3D_double(0, 0, 0);
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::segment_bottom() const
{
    if(size() > 1)
    {
        LaserBoy_3D_double bottom_;
        bottom_.y = DBL_MAX;
        for(u_int i = 0; i < size(); i++)
            if(at(i).y < bottom_.y)
                bottom_ = at(i);
        return bottom_;
    }
    else
        return LaserBoy_3D_double(0, 0, 0);
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::segment_right() const
{
    if(size() > 1)
    {
        LaserBoy_3D_double right_;
        right_.x = -DBL_MAX;
        for(u_int i = 0; i < size(); i++)
            if(at(i).x > right_.x)
                right_ = at(i);
        return right_;
    }
    else
        return LaserBoy_3D_double(0, 0, 0);
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::segment_left() const
{
    if(size() > 1)
    {
        LaserBoy_3D_double left_;
        left_.x = DBL_MAX;
        for(u_int i = 0; i < size(); i++)
            if(at(i).x < left_.x)
                left_ = at(i);
        return left_;
    }
    else
        return LaserBoy_3D_double(0, 0, 0);
}

//############################################################################
double LaserBoy_real_segment::max_x() const
{
    if(size() > 1)
    {
        double max = -DBL_MAX;
        for(u_int i = 0; i < size(); i++)
            if(max <= at(i).x)
                max = at(i).x;
        return max;
    }
    else
        return 0.0;
}

//############################################################################
double LaserBoy_real_segment::max_y() const
{
    if(size() > 1)
    {
        double max = -DBL_MAX;
        for(u_int i = 0; i < size(); i++)
            if(max <= at(i).y)
                max = at(i).y;
        return max;
    }
    else
        return 0.0;
}

//############################################################################
double LaserBoy_real_segment::max_z() const
{
    if(size() > 1)
    {
        double max = -DBL_MAX;
        for(u_int i = 0; i < size(); i++)
            if(max <= at(i).z)
                max = at(i).z;
        return max;
    }
    else
        return 0.0;
}

//############################################################################
double LaserBoy_real_segment::min_x() const
{
    if(size() > 1)
    {
        double min = DBL_MAX;
        for(u_int i = 0; i < size(); i++)
            if(min >= at(i).x)
                min = at(i).x;
        return min;
    }
    else
        return 0.0;
}

//############################################################################
double LaserBoy_real_segment::min_y() const
{
    if(size() > 1)
    {
        double min = DBL_MAX;
        for(u_int i = 0; i < size(); i++)
            if(min >= at(i).y)
                min = at(i).y;
        return min;
    }
    else
        return 0.0;
}

//############################################################################
double LaserBoy_real_segment::min_z() const
{
    if(size() > 1)
    {
        double min = DBL_MAX;
        for(u_int i = 0; i < size(); i++)
            if(min >= at(i).z)
                min = at(i).z;
        return min;
    }
    else
        return 0.0;
}

//############################################################################
double LaserBoy_real_segment::height() const
{
    return max_y() - min_y();
}
//############################################################################
double LaserBoy_real_segment::width() const
{
    return max_x() - min_x();
}

//############################################################################
double LaserBoy_real_segment::depth() const
{
    return max_z() - min_z();
}

//############################################################################
double LaserBoy_real_segment::vector_angle(u_int vertex_index) const
{
    double angle = 0.0;
    if(vertex_index != 0)
    {
        if(size() > 1)
        {
            int _0  = 0,
                _1  = 0,
                _2  = 0;
           //-----------------------------------------------------------------
            if(vertex_index > 0 && vertex_index < size() - 1)
            {
                _0 = vertex_index - 1;
                _1 = vertex_index    ;
                _2 = vertex_index + 1;
            }
           //-----------------------------------------------------------------
            else if(vertex_index == size() - 1)
            {
                _0 = size() - 2;
                _1 = 0                     ;
                _2 = 1                     ;
            }
            //----------------------------------------------------------------
            angle = delta_angle( at(_0) | at(_2), // distance between vertices 0,2
                                 at(_0) | at(_1),
                                 at(_1) | at(_2)
                               );
        }
    }
    return angle;
}

//############################################################################
double LaserBoy_real_segment::max_dimension() const
{
    double max = 0;
    if(width () > max)    max = width ();
    if(height() > max)    max = height();
    if(depth () > max)    max = depth ();
    return max;
}

//############################################################################
double LaserBoy_real_segment::max_distance_from_origin() const
{
    if(size() > 1)
    {
        double             max = 0.0,
                           d;
        LaserBoy_3D_double origin;
        for(u_int i = 0; i < size(); i++)
        {
            d = origin | at(i);
            if(d > max)
                max = d;
        }
        return max;
    }
    else
        return 0.0;
}

//############################################################################
double LaserBoy_real_segment::max_distance_from_origin_xy() const
{
    if(size() > 1)
    {
        double             max = 0.0,
                           d;
        LaserBoy_3D_double origin,
                           vertex;
        for(u_int i = 0; i < size(); i++)
        {
            vertex = at(i);
            vertex.z = 0;
            d = origin | vertex;
            if(d > max)
                max = d;
        }
        return max;
    }
    else
        return 0.0;
}

//############################################################################
double LaserBoy_real_segment::max_distance_from_origin_zy() const
{
    if(size() > 1)
    {
        double             max = 0.0,
                           d;
        LaserBoy_3D_double origin,
                           vertex;
        for(u_int i = 0; i < size(); i++)
        {
            vertex = at(i);
            vertex.x = 0;
            d = origin | vertex;
            if(d > max)
                max = d;
        }
        return max;
    }
    else
        return 0.0;
}

//############################################################################
double LaserBoy_real_segment::max_distance_from_origin_xz() const
{
    if(size() > 1)
    {
        double             max = 0.0,
                           d;
        LaserBoy_3D_double origin,
                           vertex;
        for(u_int i = 0; i < size(); i++)
        {
            vertex = at(i);
            vertex.y = 0;
            d = origin | vertex;
            if(d > max)
                max = d;
        }
        return max;
    }
    else
        return 0.0;
}

//############################################################################
u_int LaserBoy_real_segment::i_max_distance_from_origin_xy() const
{
    u_int index = 0;
    if(size() > 1)
    {        
        double             max = 0.0,
                           d;
        LaserBoy_3D_double origin,
                           vertex;
        for(u_int i = 0; i < size(); i++)
        {
            vertex = at(i);
            vertex.z = 0;
            d = origin | vertex;
            if(d > max)
            {
                index = i;
                max = d;
            }
        }
    }
    return index;
}

//############################################################################
u_int LaserBoy_real_segment::i_max_distance_from_origin_zy() const
{
    u_int index = 0;
    if(size() > 1)
    {
        double             max = 0.0,
                           d;
        LaserBoy_3D_double origin,
                           vertex;
        for(u_int i = 0; i < size(); i++)
        {
            vertex = at(i);
            vertex.x = 0;
            d = origin | vertex;
            if(d > max)
            {
                index = i;
                max = d;
            }
        }        
    }
    return index;
}

//############################################################################
u_int LaserBoy_real_segment::i_max_distance_from_origin_xz() const
{
    u_int index = 0;
    if(size() > 1)
    {        
        double             max = 0.0,
                           d;
        LaserBoy_3D_double origin,
                           vertex;
        for(u_int i = 0; i < size(); i++)
        {
            vertex = at(i);
            vertex.y = 0;
            d = origin | vertex;
            if(d > max)
            {
                index = i;
                max = d;
            }
        }
    }
    return index;
}

//############################################################################
double LaserBoy_real_segment::min_distance_from_origin() const
{
    if(size() > 1)
    {
        double             min = DBL_MAX,
                           d;
        LaserBoy_3D_double origin;
        for(u_int i = 0; i < size(); i++)
        {
            d = origin | at(i);
            if(d == 0.0)
                return 0.0;
            if(d < min)
                min = d;
        }
        return min;
    }
    else
        return 0.0;
}

//############################################################################
double LaserBoy_real_segment::min_distance_from_origin_xy() const
{
    if(size() > 1)
    {
        double             min = DBL_MAX,
                           d;
        LaserBoy_3D_double origin,
                           vertex;
        for(u_int i = 0; i < size(); i++)
        {
            vertex = at(i);
            vertex.z = 0;
            d = origin | vertex;
            if(d == 0.0)
                return 0.0;
            if(d < min)
                min = d;
        }
        return min;
    }
    else
        return 0.0;
}

//############################################################################
double LaserBoy_real_segment::min_distance_from_origin_zy() const
{
    if(size() > 1)
    {
        double             min = DBL_MAX,
                           d;
        LaserBoy_3D_double origin,
                           vertex;
        for(u_int i = 0; i < size(); i++)
        {
            vertex = at(i);
            vertex.x = 0;
            d = origin | vertex;
            if(d == 0.0)
                return 0.0;
            if(d < min)
                min = d;
        }
        return min;
    }
    else
        return 0.0;
}

//############################################################################
double LaserBoy_real_segment::min_distance_from_origin_xz() const
{
    if(size() > 1)
    {
        double             min = DBL_MAX,
                           d;
        LaserBoy_3D_double origin,
                           vertex;
        for(u_int i = 0; i < size(); i++)
        {
            vertex = at(i);
            vertex.y = 0;
            d = origin | vertex;
            if(d == 0.0)
                return 0.0;
            if(d < min)
                min = d;
        }
        return min;
    }
    else
        return 0.0;
}

//############################################################################
u_int LaserBoy_real_segment::i_min_distance_from_origin_xy() const
{
    u_int index = 0;
    if(size() > 1)
    {
        
        double             min = DBL_MAX,
                           d;
        LaserBoy_3D_double origin,
                           vertex;
        for(u_int i = 0; i < size(); i++)
        {
            vertex = at(i);
            vertex.z = 0;
            d = origin | vertex;
            if(d == 0.0)
                return 0.0;
            if(d < min)
            {
                index = i;
                min = d;
            }
        }
    }
    return index;
}

//############################################################################
u_int LaserBoy_real_segment::i_min_distance_from_origin_zy() const
{
    u_int index = 0;
    if(size() > 1)
    {
        
        double             min = DBL_MAX,
                           d;
        LaserBoy_3D_double origin,
                           vertex;
        for(u_int i = 0; i < size(); i++)
        {
            vertex = at(i);
            vertex.x = 0;
            d = origin | vertex;
            if(d == 0.0)
                return 0.0;
            if(d < min)
            {
                index = i;
                min = d;
            }
        }
    }
    return index;
}

//############################################################################
u_int LaserBoy_real_segment::i_min_distance_from_origin_xz() const
{
    u_int index = 0;
    if(size() > 1)
    {
        
        double             min = DBL_MAX,
                           d;
        LaserBoy_3D_double origin,
                           vertex;
        for(u_int i = 0; i < size(); i++)
        {
            vertex = at(i);
            vertex.y = 0;
            d = origin | vertex;
            if(d == 0.0)
                return 0.0;
            if(d < min)
            {
                index = i;
                min = d;
            }
        }
    }
    return index;
}

//############################################################################
bool LaserBoy_real_segment::segment_passes_through_origin(const double& granularity) const
{
    if(size() > 1)
    {
        LaserBoy_real_segment rs(*this);
        rs.normalize_vectors_with_origin();
        for(u_int i = 1; i < rs.size(); i++)
            if(vector_passes_through_origin(at(i - 1), at(i), granularity))
                return true;
    }
    return false;
}

//############################################################################
void LaserBoy_real_segment::blank_all_vertices()
{
    if(size() > 1)
        for(u_int i = 0; i < size(); i++)
            at(i).blank();
    return;
}

//############################################################################
void LaserBoy_real_segment::unblank_all_vertices()
{
    if(size() > 1)
        for(u_int i = 1; i < size(); i++)
            at(i).unblank();
    return;
}

//############################################################################
void LaserBoy_real_segment::strip_color() // to white in default palette
{
    palette_index = LASERBOY_ILDA_DEFAULT;
    if(size() > 1)
    {
        for(u_int i = 0; i < size(); i++)
            if(at(i).is_color(p_space->black_level))
            {
                at(i).c = 55;
                at(i).r = 255;
                at(i).g = 255;
                at(i).b = 255;
            }
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::strip_color_rgb(const LaserBoy_color& c)
{
    palette_index = LASERBOY_TRUE_COLOR;
    for(u_int i = 1; i < size(); i++)
        if(at(i).is_color(p_space->black_level))
        {
            at(i).r = c.r;
            at(i).g = c.g;
            at(i).b = c.b;
        }
    sync_rgb_and_palette();
    return;
}

//############################################################################
LaserBoy_real_segment& LaserBoy_real_segment::reverse()
{
    if(size() > 1)
    {
        int                    i;
        LaserBoy_real_segment  reversed(p_space, palette_index, false);
        LaserBoy_real_vertex   vertex;
        //----------------------------------------------------------------
        reversed.reserve(size());
        //----------------------------------------------------------------
        i = size() - 1;
        //----------------------------------------------------------------
        vertex = at(i--);
        vertex.blank();
        reversed.push_back(vertex);
        //----------------------------------------------------------------
        while(i >= 0)
        {
            vertex   = at(i    );
            vertex.r = at(i + 1).r;
            vertex.g = at(i + 1).g;
            vertex.b = at(i + 1).b;
            vertex.c = at(i + 1).c;
            vertex.k = at(i + 1).k;
            reversed.push_back(vertex);
            i--;
        }
        //----------------------------------------------------------------
        *this = reversed;
    }
    return *this;
}

//############################################################################
LaserBoy_real_segment& LaserBoy_real_segment::reorder_from(u_int vertex_index)
{
    if(    vertex_index
        && vertex_index < size()
        && size() > 1
      )
    {
        u_int                  i;
        LaserBoy_real_segment  reordered(p_space, palette_index, false);
        //----------------------------------------------------------------
        for(i = vertex_index; i < size(); i++)
            reordered += at(i);
        reordered.front().blank();
        for(i = 0; i <= vertex_index; i++)
            reordered += at(i);
        *this = reordered;
        //----------------------------------------------------------------
    }
    return *this;
}

//############################################################################
void LaserBoy_real_segment::flip(u_int plane)
{
    if(size() > 1)
    {
        u_int i;
        switch(plane)
        {
           case 0: // X mirror
                   for(i = 0; i < size(); i++)
                       at(i).x = -at(i).x;
                   break;
           //-----------------------------------------------------------------
           case 1: // Y flip
                   for(i = 0; i < size(); i++)
                       at(i).y = -at(i).y;
                   break;
           //-----------------------------------------------------------------
           case 2: // Z
                   for(i = 0; i < size(); i++)
                       at(i).z = -at(i).z;
                   break;
           //-----------------------------------------------------------------
           case 3: // X, Y
                   for(i = 0; i < size(); i++)
                   {
                       at(i).x = -at(i).x;
                       at(i).y = -at(i).y;
                   }
                   break;
           //-----------------------------------------------------------------
           case 4: // X, Y, Z
                   for(i = 0; i < size(); i++)
                       at(i) = -at(i);
                   break;
           //-----------------------------------------------------------------
        }
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::quarter_turn(u_int plane, u_int turns)
{
    if(size() > 1)
    {
        u_int   i,
                j;
        double  temp;
        switch(plane)
        {
           case 0: // X Y
                   for(i = 0; i < size(); i++)
                       for(j = 0; j < turns; j++)
                       {
                           temp = -at(i).x;
                           at(i).x = at(i).y;
                           at(i).y = temp;
                       }
                   break;
           //-----------------------------------------------------------------
           case 1: // Z Y
                   for(i = 0; i < size(); i++)
                       for(j = 0; j < turns; j++)
                       {
                           temp = -at(i).z;
                           at(i).z = at(i).y;
                           at(i).y = temp;
                       }
                   break;
           //-----------------------------------------------------------------
           case 2: // X Z
                   for(i = 0; i < size(); i++)
                       for(j = 0; j < turns; j++)
                       {
                           temp = -at(i).x;
                           at(i).x = at(i).z;
                           at(i).z = temp;
                       }
                   break;
           //-----------------------------------------------------------------
        }
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::flatten_z()
{
    if(size() > 1)
    {
        for(u_int i = 0; i < size(); i++)
            at(i).z = 0;
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::rotate(LaserBoy_3D_double a)
{
    if(size() > 1)
    {
        LaserBoy_3D_double center = centroid_of_coordinates();
        for(u_int i = 0; i < size(); i++)
            rotate_vertex_on_coordinates(at(i), center, a);
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::rotate_around_origin(LaserBoy_3D_double a)
{
    if(size() > 1)
    {
        for(u_int i = 0; i < size(); i++)
            at(i) = rotate_vertex(at(i), a);
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::rotate_on_coordinates(LaserBoy_3D_double p, LaserBoy_3D_double a)
{
    if(size() > 1)
    {
        for(u_int i = 0; i < size(); i++)
            at(i) = rotate_vertex_on_coordinates(at(i), p, a);
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::rotate_on_coordinates_x(LaserBoy_3D_double p, double a)
{
    if(size() > 1)
    {
        for(u_int i = 0; i < size(); i++)
            rotate_vertex_on_coordinates_x(at(i), p, a);
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::rotate_on_coordinates_y(LaserBoy_3D_double p, double a)
{
    if(size() > 1)
    {
        for(u_int i = 0; i < size(); i++)
            rotate_vertex_on_coordinates_y(at(i), p, a);
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::rotate_on_coordinates_z(LaserBoy_3D_double p, double a)
{
    if(size() > 1)
    {
        for(u_int i = 0; i < size(); i++)
            rotate_vertex_on_coordinates_z(at(i), p, a);
    }
    return;
}

//############################################################################
bool LaserBoy_real_segment::find_rgb_in_palette(const LaserBoy_palette& palette)
{
    bool all_colors_found = true;
    if(size() > 1)
    {
        u_int         i,
                      j;
        vector<bool>  match(size(), false);
        //--------------------------------------------------------------------
        for(i = 0; i < size(); i++)
            for(j = 0; j < palette.number_of_colors(); j++)
                if((LaserBoy_color)at(i) == palette.at(j))
                {
                    at(i).c = (u_char)j;
                    match[i] = true;
                }
        //--------------------------------------------------------------------
        for(i = 0; i < size(); i++)
            all_colors_found &= match[i];
        //--------------------------------------------------------------------
    }    
    return all_colors_found;
}

//############################################################################
void LaserBoy_real_segment::set_rgb_from_palette()
{
    if(size() > 1)        
    {
        front().r =
        front().g =
        front().b = 0x00;
        for(u_int i = 0; i < size(); i++)
        {
            if(at(i).is_color(p_space->black_level))
            {
                at(i).r = p_space->palette_picker(palette_index)[at(i).c].r;
                at(i).g = p_space->palette_picker(palette_index)[at(i).c].g;
                at(i).b = p_space->palette_picker(palette_index)[at(i).c].b;
            }
            else if(at(i).is_black(p_space->black_level))
            {
                at(i).r = 0;
                at(i).g = 0;
                at(i).b = 0;
            }
        }
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::set_palette_to_332()
{
    if(size() > 1)
    {
        front().c = 0x00;
        for(u_int i = 1; i < size(); i++)
            at(i).c =    (at(i).r & 0xe0)
                      | ((at(i).g & 0xe0) >> 3)
                      | ((at(i).b & 0xc0) >> 6);
        // does NOT set palette_index to LASERBOY_REDUCED_332
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::sync_rgb_and_palette()
{
    if(size() > 1)
    {
        bool fits_in_default = true;
        int  i;
        if(palette_index != LASERBOY_TRUE_COLOR)
        {
            for(i = 0; i < (int)p_space->palette_picker(palette_index).size(); i++)
                fits_in_default &= p_space->palette_picker(LASERBOY_ILDA_DEFAULT).has_color(p_space->palette_picker(palette_index).at(i));
            if(fits_in_default)
            {
                best_match_palette(LASERBOY_ILDA_DEFAULT);
                return;
            }
            set_rgb_from_palette();
            return;
        }
        //----------------------------------------------------------------
        u_int             j;
        LaserBoy_palette  palette(p_space),
                          reordered(p_space);
        //----------------------------------------------------------------
        if(first_lit_vector_index() > -1) // there are lit vectors!
        {
            palette.push_back((LaserBoy_color)at(first_lit_vector_index()));
            //----------------------------------------------------------------
            for(i = first_lit_vector_index() + 1; i < (int)size(); i++)
            {
                for(j = 0; j < palette.number_of_colors(); j++)
                    if(at(i).is_lit() && (palette[j] == (LaserBoy_color)at(i)))
                        break;
                if(    j == palette.number_of_colors()
                    && at(i).is_lit()
                  )
                    palette.push_back((LaserBoy_color)at(i));
                if(palette.number_of_colors() > LASERBOY_PALETTE_MAX)
                {
                    set_palette_to_332();
                    return;
                }
            }
            //----------------------------------------------------------------
            for(i = 0; i < (int)palette.size(); i++)
                fits_in_default &= p_space->palette_picker(LASERBOY_ILDA_DEFAULT).has_color(palette.at(i));
            if(fits_in_default)
            {
                best_match_palette(LASERBOY_ILDA_DEFAULT);
                return;
            }
            palette.reorder();
            for(i = p_space->number_of_palettes() - 1; i >= 0; i--)
            {
                reordered = p_space->palette_picker(i);
                reordered.reorder();
                if(palette.is_in(reordered))
                {
                    palette_index = i;
                    find_rgb_in_palette(p_space->palette_picker(i));
                    return;
                }
            }
            palette.find_factors();
            find_rgb_in_palette(palette);
            palette.name = p_space->GUID8char();
            p_space->push_back_palette(palette);
            palette_index = p_space->number_of_palettes() - 1;
            return;
        } // end if(first_lit_vector_index() > -1) // there are lit vectors!
        palette_index = LASERBOY_ILDA_DEFAULT;
        //----------------------------------------------------------------
    } // end if(size() > 1)
    return;
}

//############################################################################
void LaserBoy_real_segment::best_match_palette(int index)
{
    if(    index != palette_index
        && index < p_space->number_of_palettes()
      )
    {
        if(size() > 1)
        {
            if(!p_space->allow_lit_black)
                convert_black_to_blank();
            else
                impose_black_level();

            for(u_int i = 1; i < size(); i++)
                 at(i).c = p_space->
                           palette_picker(index)
                           .best_match(at(i));
        }
        palette_index = index;
        set_rgb_from_palette();
        p_space->palette_index = palette_index;
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::bit_reduce_to_palette()
{
    if(palette_index == LASERBOY_TRUE_COLOR)
    {
        set_palette_to_332();
        palette_index = LASERBOY_REDUCED_332;
        set_rgb_from_palette();
    }
    return;
}


//############################################################################
void LaserBoy_real_segment::best_reduce_to_palette()
{
    if(number_of_color_vectors() >= 1)
    {
        int               local_palette_index;
        u_int             i,
                          j;
        LaserBoy_palette  palette(p_space);
        //----------------------------------------------------------------
        if(!p_space->allow_lit_black)
            convert_black_to_blank();
        else
            impose_black_level();
        //----------------------------------------------------------------
        palette.push_back((LaserBoy_color)at(first_lit_vector_index()));
        //----------------------------------------------------------------
        for(i = first_lit_vector_index() + 1; i < size(); i++)
        {
            for(j = 0; j < palette.number_of_colors(); j++)
                if(at(i).is_lit() && (palette[j] == (LaserBoy_color)at(i)))
                    break;
            if(    j == palette.number_of_colors()
                && at(i).is_lit()
              )
                palette.push_back((LaserBoy_color)at(i));
        }
        //----------------------------------------------------------------
        local_palette_index = palette.best_reduction();
        if(local_palette_index == -1)
        {
            palette.name = p_space->GUID8char();
            p_space->push_back_palette(palette);
            best_match_palette(p_space->number_of_palettes() - 1);
        }
        else
            best_match_palette(local_palette_index);
        //----------------------------------------------------------------
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::convert_black_to_blank()
{
    if(size() > 1)    
        for(u_int i = 0; i < size(); i++)
        {
            if(at(i).is_black(p_space->black_level))
            {
                at(i).r = 255;
                at(i).g = 0;
                at(i).b = 0;
                at(i).c = 0;
                at(i).blank();
            }
        }
    return;
}

//############################################################################
void LaserBoy_real_segment::convert_blank_to_black()
{
    if(size() > 1)    
        for(u_int i = 0; i < size(); i++)
        {
            if(at(i).is_blank())
            {
                at(i).r = 0;
                at(i).g = 0;
                at(i).b = 0;
                at(i).c = p_space->palette_picker(palette_index).black;
                at(i).unblank();
            }
        }
    return;
}

//############################################################################
void LaserBoy_real_segment::impose_black_level()
{
    if(size() > 1)    
        for(u_int i = 0; i < size(); i++)
        {
            if(at(i).as_LaserBoy_color().average() < p_space->black_level)
            {
                at(i).r = 0;
                at(i).g = 0;
                at(i).b = 0;
            }
        }
    return;
}

//############################################################################
void LaserBoy_real_segment::reduce_blank_vectors()
{
    if(size() > 1)
    {
        u_int                  i;
        LaserBoy_real_segment  minimum_blanking(p_space, palette_index, false);
//        minimum_blanking.reserve(size());
        minimum_blanking.push_back(front());
        minimum_blanking.back().blank();
        for(i = 1; i < size(); i++)
        {
            if(    at(i).is_blank()
                && minimum_blanking.back().is_blank()
              )
                minimum_blanking.back() = at(i); // the new, last blank
            else
                minimum_blanking.push_back(at(i));
        }
        while(minimum_blanking.size() && minimum_blanking.back().is_blank())
            minimum_blanking.pop_back();
        *this = minimum_blanking;
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::reduce_lit_vectors()
{
    if(size() > 1)
    {
        u_int             i;
        LaserBoy_real_segment  segment_1(p_space, palette_index, false),
                               segment_2(p_space, palette_index, false);
        //----------------------------------------------------------------
        segment_1.reserve(size());
        segment_2.reserve(size());
        //----------------------------------------------------------------
        segment_1.push_back(front());
        for(i = 1; i < size(); i++)
        {
            if(at(i) != segment_1.back())
                segment_1.push_back(at(i));
        }
        //----------------------------------------------------------------
        while(segment_1.size() && segment_1.back().is_blank())
            segment_1.pop_back();
        //----------------------------------------------------------------
        if(segment_1.size())
        {
            segment_2.push_back(segment_1.front());
            for(i = 1; i < segment_1.size() - 1; i++)
            {
                if(    (    segment_1.at(i    ).color_of(p_space->black_level)
                         != segment_1.at(i + 1).color_of(p_space->black_level)
                       )
                    || (    segment_1.vector_angle(i)
                          > p_space->insignificant_angle
                       )
                  )
                    segment_2.push_back(segment_1.at(i));
            }
            //----------------------------------------------------------------
            segment_2.push_back(segment_1.at(i));
            while(segment_2.size() && segment_2.back().is_blank())
                segment_2.pop_back();
        }
        //----------------------------------------------------------------
        *this = segment_2;
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::omit_equivalent_vectors()
{
    if(number_of_segments() > 1)
    {
        LaserBoy_real_segment_set  segments(p_space);
        u_int                      i,
                                   j;
        //----------------------------------------------------------------
        reduce_blank_vectors();
        reduce_lit_vectors();
        fracture_segments();
        segments = explode_segments();
        //----------------------------------------------------------------
        for(i = 0; i < segments.size() - 1; i++)
        {
            for(j = i + 1; j < segments.size(); j++)
                if(    segments[i] == segments[j]
                    || segments[i] == segments[j].reverse()
                  )
                    segments[j].clear();
        }
        //----------------------------------------------------------------
        *this = segments.sum_of_frames();
        //----------------------------------------------------------------
    }
    return;
}

//############################################################################
LaserBoy_real_segment& LaserBoy_real_segment::move(LaserBoy_3D_double d)
{
    if(size() > 1)    
        for(u_int i = 0; i < size(); i++)
            at(i) += d;
    return *this;
}

//############################################################################
LaserBoy_real_segment& LaserBoy_real_segment::scale(LaserBoy_3D_double s)
{
    scale_on_coordinates(centroid_of_coordinates(), s);
    return *this;
}

//############################################################################
LaserBoy_Error_Code LaserBoy_real_segment::add_lit_span_vertices()
{
    if(size() > 1)
    {
        u_int                  i;
        LaserBoy_real_segment  distance_optimized(p_space, palette_index, false),
                               line              (p_space, palette_index, false);
        //----------------------------------------------------------------
        for(i = 0; i < (size() - 1); i++)
        {
            distance_optimized.push_back(at(i));
            if(    at(i + 1).is_lit()
                && (at(i) | at(i + 1)) > p_space->lit_delta_max
              )
            {
                line += LaserBoy_real_segment(p_space, at(i), at(i + 1));
                line.pop_back();
                distance_optimized += line;
                line.clear();
            }
        }
        distance_optimized.push_back(back());
        //----------------------------------------------------------------
        *this = distance_optimized;
    }
    return real_segment_error;
}

//############################################################################
void LaserBoy_real_segment::add_lit_span_vertices(const u_int& max_d)
{
    if(size() > 1)
    {
        u_int                  i;
        LaserBoy_real_segment  distance_optimized(p_space, palette_index, false),
                               line              (p_space, palette_index, false);
        //----------------------------------------------------------------
        for(i = 0; i < (size() - 1); i++)
        {
            distance_optimized.push_back(at(i));
            if(    at(i + 1).is_lit()
                && (at(i) | at(i + 1)) > max_d
              )
            {
                line += LaserBoy_real_segment(p_space, at(i), at(i + 1), max_d);
                line.pop_back();
                distance_optimized += line;
                line.clear();
            }
        }
        distance_optimized.push_back(back());
        *this = distance_optimized;
    }
    return;
}

//############################################################################
LaserBoy_Error_Code LaserBoy_real_segment::add_blank_span_vertices()
{
    if(size() > 1)
    {
        u_int                  i;
        LaserBoy_real_segment  distance_optimized(p_space, palette_index, false),
                               line              (p_space, palette_index, false);
        //----------------------------------------------------------------
        for(i = 0; i < (size() - 1); i++)
        {
            distance_optimized.push_back(at(i));
            if(    at(i + 1).is_blank()
                && (at(i) | at(i + 1)) > p_space->blank_delta_max
              )
            {
                line += LaserBoy_real_segment(p_space, at(i), at(i + 1));
                line.pop_back();
                distance_optimized += line;
                line.clear();
            }
        }
        distance_optimized.push_back(back());
        //----------------------------------------------------------------
        *this = distance_optimized;
    }
    return real_segment_error;
}

//############################################################################
void LaserBoy_real_segment::scale_on_coordinates(LaserBoy_3D_double p, LaserBoy_3D_double s)
{
    if(size() > 1)    
        for(u_int i = 0; i < size(); i++)
            at(i) = ((at(i).as_3D_double() - p) * s) + p;
    return;
}

//############################################################################
void LaserBoy_real_segment::scale_around_origin(LaserBoy_3D_double s)
{
    if(size() > 1)    
        for(u_int i = 0; i < size(); i++)
            at(i) *= s;
    return;
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::rectangular_center_of() const
{
    LaserBoy_3D_double center;
    if(size() > 1)
    {
        double  max_x = -DBL_MAX,
                min_x =  DBL_MAX,
                max_y = -DBL_MAX,
                min_y =  DBL_MAX,
                max_z = -DBL_MAX,
                min_z =  DBL_MAX;

        for(u_int i = 0; i < size(); i++)
        {
            if(at(i).x > max_x)    max_x = at(i).x;
            if(at(i).x < min_x)    min_x = at(i).x;
            if(at(i).y > max_y)    max_y = at(i).y;
            if(at(i).y < min_y)    min_y = at(i).y;
            if(at(i).z > max_z)    max_z = at(i).z;
            if(at(i).z < min_z)    min_z = at(i).z;
        }
        center.x = (max_x - ((max_x - min_x) / 2.0));
        center.y = (max_y - ((max_y - min_y) / 2.0));
        center.z = (max_z - ((max_z - min_z) / 2.0));
    }
    return center;
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::mean_of_coordinates() const
{
    LaserBoy_3D_double mean;
    if(size() > 1)
    {
        mean = front();
        for(u_int i = 1; i < size(); i++)
            mean += at(i);
        mean /= size();
    }
    return mean;
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::centroid_of_coordinates_xy(double& area, LaserBoy_real_segment& rs) const
{
    LaserBoy_3D_double centroid;
    area = 0.0;
    rs.clear();
    if(size() > 1)
    {
        u_int i;
        rs = polygon_outline_xy(area);
        if(area == 0.0) // from this perspective the segment is either a dot or a line
            return centroid;
        for(i = 0; i < rs.size() - 1; i++)
        {
            centroid.x += (rs.at(i).x + rs.at(i + 1).x) * ((rs.at(i).x * rs.at(i + 1).y) - (rs.at(i + 1).x * rs.at(i).y));
            centroid.y += (rs.at(i).y + rs.at(i + 1).y) * ((rs.at(i).x * rs.at(i + 1).y) - (rs.at(i + 1).x * rs.at(i).y));
        }
        centroid *= (1.0 / (area * 6.0));
        return centroid;
    }
    return centroid;
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::centroid_of_coordinates_zy(double& area, LaserBoy_real_segment& rs) const
{
    LaserBoy_3D_double centroid;
    area = 0.0;
    rs.clear();
    if(size() > 1)
    {
        u_int i;
        rs = polygon_outline_zy(area);
        if(area == 0.0) // from this perspective the segment is either a dot or a line
            return centroid;
        for(i = 0; i < rs.size() - 1; i++)
        {
            centroid.z += (rs.at(i).z + rs.at(i + 1).z) * ((rs.at(i).z * rs.at(i + 1).y) - (rs.at(i + 1).z * rs.at(i).y));
            centroid.y += (rs.at(i).y + rs.at(i + 1).y) * ((rs.at(i).z * rs.at(i + 1).y) - (rs.at(i + 1).z * rs.at(i).y));
        }
        centroid *= (1.0 / (area * 6.0));
        return centroid;
    }
    return centroid;
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::centroid_of_coordinates_xz(double& area, LaserBoy_real_segment& rs) const
{
    LaserBoy_3D_double centroid;
    area = 0.0;
    rs.clear();
    if(size() > 1)
    {
        u_int i;
        rs = polygon_outline_xz(area);
        if(area == 0.0) // from this perspective the segment is either a dot or a line
            return centroid;
        for(i = 0; i < rs.size() - 1; i++)
        {
            centroid.x += (rs.at(i).x + rs.at(i + 1).x) * ((rs.at(i).x * rs.at(i + 1).z) - (rs.at(i + 1).x * rs.at(i).z));
            centroid.z += (rs.at(i).z + rs.at(i + 1).z) * ((rs.at(i).x * rs.at(i + 1).z) - (rs.at(i + 1).x * rs.at(i).z));
        }
        centroid *= (1.0 / (area * 6.0));
        return centroid;
    }
    return centroid;
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::centroid_of_coordinates() const
{
    LaserBoy_real_segment rs(*this);
    rs.reduce_blank_vectors();
    rs.strip_color();
    rs.reduce_lit_vectors();
    LaserBoy_3D_double    centroid = rs.rectangular_center_of();
    if((rs.size() > 2) && rs.max_dimension())
    {
        double                area_xy,
                              area_xz,
                              area_zy;
        LaserBoy_real_segment polygon_xy(p_space),
                              polygon_xz(p_space),
                              polygon_zy(p_space);
        LaserBoy_3D_double centroid_xy = rs.centroid_of_coordinates_xy(area_xy, polygon_xy),
                           centroid_xz = rs.centroid_of_coordinates_xz(area_xz, polygon_xz),
                           centroid_zy = rs.centroid_of_coordinates_zy(area_zy, polygon_zy);
        if(area_xy != 0.0 || area_xz != 0.0)
            centroid.x =    (   centroid_xy.x * area_xy
                              + centroid_xz.x * area_xz
                            )
                          / (area_xy + area_xz);
        if(area_xy != 0.0 || area_zy != 0.0)
            centroid.y =    (   centroid_xy.y * area_xy
                              + centroid_zy.y * area_zy
                            )
                          / (area_xy + area_zy);
        if(area_zy != 0.0 || area_xz != 0.0)
            centroid.z =    (   centroid_zy.z * area_zy
                              + centroid_xz.z * area_xz
                            )
                          / (area_zy + area_xz);
    }
    return centroid;
}

//############################################################################
LaserBoy_real_segment LaserBoy_real_segment::polygon_outline_xy(double& area) const
{
    LaserBoy_real_segment rs(p_space);
    area = 0.0;
    if((width() == 0.0) || (height() == 0.0))
        return rs;
    if(size() > 2)
    {
        bool               cancave_found;
        u_int              i,
                           j;
        LaserBoy_3D_double origin, 
                           center;
        rs = (*this);
        for(i = 0; i < rs.size(); i++)
            rs.at(i).z = 0.0; // 2D
        rs.reduce_blank_vectors();
        rs.strip_color();
        rs.reduce_lit_vectors();
        if(rs.size() < 3)
        {
            rs.clear();
            return rs;
        }
        for(i = 0; i < rs.size(); i++)
            rs.at(i).unblank();
        center = rs.rectangular_center_of();
        rs -= center;
        rs.sort_by_rotation_xy();
        rs.reorder_from(rs.i_max_distance_from_origin_xy());
        rs.push_back(rs.front());
        do
        {
            cancave_found = false;
            for(i = 0; i < rs.size() - 2; i++)
                if(three_point_angle_xy(rs.at(i + 1), rs.at(i), rs.at(i + 2)) <= pi)
                {
                    cancave_found = true;
                    rs.remove_vertex(i + 1);
                    break;
                }
        } while(cancave_found == true);
        if(rs.back().as_3D_double() == rs.front().as_3D_double())
            rs.pop_back();
        rs.sort_by_rotation_xy();
        rs.push_back(rs.front());
        rs.front().blank();
        j = rs.size() - 2;
        for(i = 0; i < rs.size() - 1; i++)
        { 
            area +=   (rs.at(j).x + rs.at(i).x) 
                    * (rs.at(j).y - rs.at(i).y);
            j = i;
        }   
        area = abs(area / 2.0);
        if(area != 0.0)
            return rs + center;
        return LaserBoy_real_segment(p_space);
    }
    return rs;
}

//############################################################################
LaserBoy_real_segment LaserBoy_real_segment::polygon_outline_zy(double& area) const
{
    LaserBoy_real_segment rs(p_space);
    area = 0.0;
    if((height() == 0.0) || (depth() == 0.0))
        return rs;
    if(size() > 2)
    {
        bool               cancave_found;
        u_int              i,
                           j;
        LaserBoy_3D_double origin, 
                           center;
        rs = (*this);
        for(i = 0; i < rs.size(); i++)
            rs.at(i).x = 0.0; // 2D
        rs.reduce_blank_vectors();
        rs.strip_color();
        rs.reduce_lit_vectors();
        if(rs.size() < 3)
        {
            rs.clear();
            return rs;
        }
        for(i = 0; i < rs.size(); i++)
            rs.at(i).unblank();
        center = rs.rectangular_center_of();
        rs -= center;
        rs.sort_by_rotation_zy();
        rs.reorder_from(rs.i_max_distance_from_origin_zy());
        rs.push_back(rs.front());
        do
        {
            cancave_found = false;
            for(i = 0; i < rs.size() - 2; i++)
                if(three_point_angle_zy(rs.at(i + 1), rs.at(i), rs.at(i + 2)) <= pi)
                {
                    cancave_found = true;
                    rs.remove_vertex(i + 1);
                    break;
                }
        } while(cancave_found == true);
        if(rs.back().as_3D_double() == rs.front().as_3D_double())
            rs.pop_back();
        rs.sort_by_rotation_zy();
        rs.push_back(rs.front());
        rs.front().blank();
        j = rs.size() - 2;
        for(i = 0; i < rs.size() - 1; i++)
        { 
            area +=   (rs.at(j).z + rs.at(i).z) 
                    * (rs.at(j).y - rs.at(i).y);
            j = i;
        }   
        area = abs(area / 2.0);
        if(area != 0.0)
            return rs + center;
        return LaserBoy_real_segment(p_space);
    }
    return rs;
}

//############################################################################
LaserBoy_real_segment LaserBoy_real_segment::polygon_outline_xz(double& area) const
{
    LaserBoy_real_segment rs(p_space);
    area = 0.0;
    if((width() == 0.0) || (depth() == 0.0))
        return rs;
    if(size() > 2)
    {
        bool               cancave_found;
        u_int              i,
                           j;
        LaserBoy_3D_double origin, 
                           center;
        rs = (*this);
        for(i = 0; i < rs.size(); i++)
            rs.at(i).y = 0.0; // 2D
        rs.reduce_blank_vectors();
        rs.strip_color();
        rs.reduce_lit_vectors();
        if(rs.size() < 3)
        {
            rs.clear();
            return rs;
        }
        for(i = 0; i < rs.size(); i++)
            rs.at(i).unblank();
        center = rs.rectangular_center_of();
        rs -= center;
        rs.sort_by_rotation_xz();
        rs.reorder_from(rs.i_max_distance_from_origin_xz());
        rs.push_back(rs.front());
        do
        {
            cancave_found = false;
            for(i = 0; i < rs.size() - 2; i++)
                if(three_point_angle_xz(rs.at(i + 1), rs.at(i), rs.at(i + 2)) <= pi)
                {
                    cancave_found = true;
                    rs.remove_vertex(i + 1);
                    break;
                }
        } while(cancave_found == true);
        if(rs.back().as_3D_double() == rs.front().as_3D_double())
            rs.pop_back();
        rs.sort_by_rotation_xz();
        rs.push_back(rs.front());
        rs.front().blank();
        j = rs.size() - 2;
        for(i = 0; i < rs.size() - 1; i++)
        { 
            area +=   (rs.at(j).x + rs.at(i).x) 
                    * (rs.at(j).z - rs.at(i).z);
            j = i;
        }   
        area = abs(area / 2.0);
        if(area != 0.0)
            return rs + center;
        return LaserBoy_real_segment(p_space);
    }
    return rs;
}

//############################################################################
u_int LaserBoy_real_segment::number_of_segments() const // a segment is a series of lit verticies
{
    u_int  i,
           segment_count = 0;
    if(size() > 1)
    {
        for(i = 1; i < size(); i++)
        {
            if(at(i).is_lit())
            {
                while(at(i).is_lit() && i < (size() - 1))
                    i++;
                segment_count++;
            }
        }
    }
    return segment_count;
}

//############################################################################
bool LaserBoy_real_segment::find_segment_at_index(u_int segment_index, u_int& start, u_int& end) const
{   // the first segment is number zero!
    if(size() > 1)
    {
        u_int  i;
        int    segment_count = -1;
        //----------------------------------------------------------------
        for(i = 1; i < size(); i++)
        {
            if(at(i).is_lit())
            {
                start = i - 1;
                while(at(i).is_lit() && i < (size() - 1))
                    i++;
                //------------------------------------------------------------
                end = i - 1;
                if(    i == (size() - 1)
                    && at(i).is_lit()
                  )
                    end = i;
                //------------------------------------------------------------
                segment_count++;
                if(segment_count == (int)segment_index)
                    return true;
            }
        } // segment index either negative or out of range
        //----------------------------------------------------------------
        for(i = 1; i < size(); i++)
        {
            if(at(i).is_lit())
            {
                start = i - 1;
                while(at(i).is_lit() && i < (size() - 1))
                    i++;
                end = i - 1;
                if(    i == (size() - 1)
                    && at(i).is_lit()
                  )
                    end = i;
                //------------------------------------------------------------
                return false; // and set start, end to first segment
            }
        }
    }
    //------------------------------------------------------------------------
    return false;
}

//############################################################################
LaserBoy_real_segment& LaserBoy_real_segment::fracture_segments()
{
    if(number_of_lit_vectors() > 1)
    {
        u_int                  i;
        LaserBoy_real_segment  fractured(p_space, palette_index, false);
        fractured.reserve(3 * size());
        reduce_blank_vectors();
        //----------------------------------------------------------------
        for(i = 0; i < size(); i++)
        {
            fractured.push_back(at(i));
            fractured.push_back(at(i));
            fractured.back().blank();
        }
        //----------------------------------------------------------------
        fractured.pop_back();
        fractured.reduce_blank_vectors();
    }
    return *this;
}

//############################################################################
LaserBoy_real_segment& LaserBoy_real_segment::bond_segments()
{
    if(size() > 1)
    {
        u_int                  i;
        LaserBoy_real_segment  bonded(p_space, palette_index, false);
        //----------------------------------------------------------------
        bonded.reserve(size());
        bonded.push_back(front());
        for(i = 1; i < size() - 1; i++)
        {
            if( !(     at(i).is_blank()
                   &&     (   at(i)
                            | at(i - 1)
                          )
                       <= p_space->insignificant_distance
              )  )
                bonded += at(i);
        }
        bonded.push_back(back());
        //----------------------------------------------------------------
    }
    return *this;
}

//############################################################################
LaserBoy_real_segment& LaserBoy_real_segment::sort_by_rotation_xy()
{
    if(size() > 1)
    {
        u_int                     i;
        LaserBoy_3D_double        origin,
                                  base_line(32767.0, 0.0, 0.0);
        LaserBoy_real_segment     rs1(*this),
                                  rs2(p_space);
        LaserBoy_4D_double_vector _v;
        for(i = 0; i < rs1.size(); i++)
        {
            rs1.at(i).z = 0.0;
            _v.push_back(LaserBoy_4D_double(rs1.at(i).as_3D_double(),
                                            three_point_angle_xy(origin, base_line, rs1.at(i))
                                           )
                        );
        }
        _v.sort();
        for(i = 0; i < _v.size(); i++)
            rs2.push_back(LaserBoy_real_vertex( _v.at(i).x,
                                                _v.at(i).y,
                                                _v.at(i).z,
                                                255,
                                                255,
                                                255,
                                                0,
                                                55
                                              )
                         );
        *this = rs2;
    }
    return *this;
}

//############################################################################
LaserBoy_real_segment& LaserBoy_real_segment::sort_by_rotation_xz()
{
    if(size() > 1)
    {
        u_int                     i;
        LaserBoy_3D_double        origin,
                                  base_line(32767.0, 0.0, 0.0);
        LaserBoy_real_segment     rs1(*this),
                                  rs2(p_space);
        LaserBoy_4D_double_vector _v;
        for(i = 0; i < rs1.size(); i++)
        {
            rs1.at(i).y = 0.0;
            _v.push_back(LaserBoy_4D_double(rs1.at(i).as_3D_double(),
                                            three_point_angle_xz(origin, base_line, rs1.at(i))
                                           )
                        );
        }
        _v.sort();
        for(i = 0; i < _v.size(); i++)
            rs2.push_back(LaserBoy_real_vertex( _v.at(i).x,
                                                _v.at(i).y,
                                                _v.at(i).z,
                                                255,
                                                255,
                                                255,
                                                0,
                                                55
                                              )
                         );
        *this = rs2;
    }
    return *this;
}

//############################################################################
LaserBoy_real_segment& LaserBoy_real_segment::sort_by_rotation_zy()
{
    if(size() > 1)
    {
        u_int                     i;
        LaserBoy_3D_double        origin,
                                  base_line(0.0, 32767.0, 0.0);
        LaserBoy_real_segment     rs1(*this),
                                  rs2(p_space);
        LaserBoy_4D_double_vector _v;
        for(i = 0; i < rs1.size(); i++)
        {
            rs1.at(i).x = 0.0;
            _v.push_back(LaserBoy_4D_double(rs1.at(i).as_3D_double(),
                                            three_point_angle_zy(origin, base_line, rs1.at(i))
                                           )
                        );
        }
        _v.sort();
        for(i = 0; i < _v.size(); i++)
            rs2.push_back(LaserBoy_real_vertex( _v.at(i).x,
                                                _v.at(i).y,
                                                _v.at(i).z,
                                                255,
                                                255,
                                                255,
                                                0,
                                                55
                                              )
                         );
        *this = rs2;
    }
    return *this;
}

//############################################################################
LaserBoy_real_segment_set LaserBoy_real_segment::explode_segments() const
{
    LaserBoy_real_segment_set segments(p_space);
    //------------------------------------------------------------------------
    if(size() > 1)
    {
        u_int                  i,
                               j,
                               start,
                               end;
        LaserBoy_real_segment  segment(p_space, palette_index, false);
        //----------------------------------------------------------------
        for(i = 1; i < size(); i++)
        {
            if(at(i).is_lit())
            {
                start = i - 1; // anchor to the first lit vertex
                while(at(i).is_lit() && i < (size() - 1))
                    i++;
                //------------------------------------------------------------
                end = i - 1;
                if(    i == (size() - 1)
                    && at(i).is_lit()
                  )
                    end = i;
                //------------------------------------------------------------
                segment.clear();
                segment.reserve(end - start + 1);
                for(j = start; j <= end; j++)
                    segment += at(j);
                segments.push_back(segment);
            }
        }
    }
    //------------------------------------------------------------------------
    return segments;
}

//############################################################################
LaserBoy_real_segment LaserBoy_real_segment::copy_segment(u_int segment_index) const
{
    LaserBoy_real_segment rs(p_space);
    rs.palette_index = palette_index;
    if(size() > 1)
    {
        u_int start, end;
        if(find_segment_at_index(segment_index, start, end))
        {
            rs.clear();
            rs.reserve(end - start + 1);
            for(u_int i = start; i <= end; i++)
                rs.push_back(at(i));
        }
    }
    return rs;
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::rectangular_center_of_segment(u_int segment_index) const
{
    return (copy_segment(segment_index)).rectangular_center_of();
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::mean_of_coordinates_of_segment(u_int segment_index) const
{
    return (copy_segment(segment_index)).mean_of_coordinates();
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::centroid_of_segment_xy(u_int segment_index, double& area, LaserBoy_real_segment& rs) const
{
    return (copy_segment(segment_index)).centroid_of_coordinates_xy(area, rs);
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::centroid_of_segment_xz(u_int segment_index, double& area, LaserBoy_real_segment& rs) const
{
    return (copy_segment(segment_index)).centroid_of_coordinates_xz(area, rs);
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::centroid_of_segment_zy(u_int segment_index, double& area, LaserBoy_real_segment& rs) const
{
    return (copy_segment(segment_index)).centroid_of_coordinates_zy(area, rs);
}

//############################################################################
LaserBoy_3D_double LaserBoy_real_segment::centroid_of_segment(u_int segment_index) const
{
    return (copy_segment(segment_index)).centroid_of_coordinates();
}

//############################################################################
void LaserBoy_real_segment::move_segment(u_int segment_index, LaserBoy_3D_double d)
{
    if(size() > 1)
    {
        u_int  i    ,
               start,
               end  ;
        find_segment_at_index(segment_index, start, end);
        //----------------------------------------------------------------
        for(i = start; i <= end; i++)
            at(i) += d;
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::rotate_segment(u_int segment_index, LaserBoy_3D_double a)
{
    if(size() > 1)
    {
        u_int               i,
                            start,
                            end;
        LaserBoy_3D_double  center = centroid_of_segment(segment_index);
        find_segment_at_index(segment_index, start, end);
        for(i = start; i <= end; i++)
            at(i) = rotate_vertex_on_coordinates(at(i), center, a);
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::rotate_segment_around_origin(u_int segment_index, LaserBoy_3D_double a)
{
    if(size() > 1)
    {
        u_int  i    ,
               start,
               end  ;
        find_segment_at_index(segment_index, start, end);
        //----------------------------------------------------------------
        for(i = start; i <= end; i++)
            at(i) = rotate_vertex(at(i), a);
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::scale_segment(u_int segment_index, LaserBoy_3D_double m)
{
    if(size() > 1)
    {
        u_int              i     ,
                           start ,
                           end   ;
        LaserBoy_3D_double center;
        //----------------------------------------------------------------
        find_segment_at_index(segment_index, start, end);
        center = centroid_of_segment(segment_index);
        //----------------------------------------------------------------
        for(i = start; i <= end; i++)
            at(i) = scale_vertex_on_coordinates(at(i), center, m);
        //----------------------------------------------------------------
    }
    return;
}

//############################################################################
void LaserBoy_real_segment::scale_segment_around_origin(u_int segment_index, LaserBoy_3D_double m)
{
    if(size() > 1)
    {
        u_int  i    ,
               start,
               end  ;
        //----------------------------------------------------------------
        find_segment_at_index(segment_index, start, end);
        //----------------------------------------------------------------
        for(i = start; i <= end; i++)
            at(i) *= m;
        //----------------------------------------------------------------
    }
    return;
}

//############################################################################
LaserBoy_Error_Code LaserBoy_real_segment::from_ifstream_dxf(std::ifstream& in)
{
    u_char                    dxf_color_index     ,
                              prev_dxf_color_index;
    u_int                     arc_vertices  = 0         ,
                              group_70_flag = 0         ,
                              group_71_flag = 0         ,
                              group_72_flag = 0         ,
                              group_73_flag = 0         ;
    int                       group_code                ,
                              first_segment_vertex_index;
    double                    double_value,
                              radius      ,
                              arc_start   ,
                              arc_end     ,
                              arc_angle   ,
                              arc_step    ;
    string                    entity_string,
                              font         ,
                              text         ;
    LaserBoy_ild_header_count counter;
    LaserBoy_3D_double        double_3D_1,
                              double_3D_2,
                              double_3D_3,
                              double_3D_4;
    LaserBoy_color            color,
                              prev_color;
    LaserBoy_real_segment     real_vertices(p_space);
    vector<double>            vertex_x,
                              vertex_y,
                              vertex_z;
    bool                      closed_polyline;
    //------------------------------------------------------------------------
    push_back(LaserBoy_real_vertex()); // every LaserBoy_real_segment::from_ifstream_dxf
    push_back(LaserBoy_real_vertex()); // has an original vector this.size() not less than 2
    //------------------------------------------------------------------------
    while(in.good() && entity_string != "ENTITIES") // ignore everthing up to ENTITIES
        in >> entity_string;
    //------------------------------------------------------------------------
    if(in.good())
    {
        while(in.good())
        {
            dxf_color_index          = p_space->palette_picker(LASERBOY_DXF).white;
            prev_dxf_color_index     = p_space->palette_picker(LASERBOY_DXF).white;
            color                      = (u_int)0X00ffffff;
            prev_color                 = (u_int)0X00ffffff;
            first_segment_vertex_index = -1,
            closed_polyline            = false;
            group_70_flag              = 0;
            radius                     = 0;
            arc_vertices               = 0;
            arc_start                  = 0;
            arc_end                    = 360;
            group_code                 = -1;
            //----------------------------------------------------------------
            vertex_x.clear();
            vertex_y.clear();
            vertex_z.clear();
            //----------------------------------------------------------------
            if(entity_string == "POINT")
            {
                while(get_dxf_pair(in, group_code, entity_string))
                {
                    sscanf(entity_string.c_str(), "%lf", &double_value);
                    switch(group_code)
                    {
                        case  10: // x1
                                    double_3D_1.x = double_value;
                                    break;
                        case  20: // y1
                                    double_3D_1.y = double_value;
                                    break;
                        case  30: // z1
                                    double_3D_1.z = double_value;
                                    break;
                        case  62: // palette_index
                                    color = p_space->palette_picker(LASERBOY_DXF)[(u_char)double_value];
                                    if(color.is_black())
                                        color = p_space->palette_picker(LASERBOY_DXF)[p_space->palette_picker(LASERBOY_DXF).white];
                                    break;
                        case 420: // true color
                                    color = (u_int)double_value;
                                    if(color.is_black())
                                        color = (u_int)0X00ffffff;
                                    break;
                    }
                }
                push_back(LaserBoy_real_vertex(double_3D_1, color).blank());
                push_back(LaserBoy_real_vertex(double_3D_1, color).unblank());
            }
            else if(entity_string == "LINE")
            {
                while(get_dxf_pair(in, group_code, entity_string))
                {
                    sscanf(entity_string.c_str(), "%lf", &double_value);
                    switch(group_code)
                    {
                        case  10: // x1
                                    double_3D_1.x = double_value;
                                    break;
                        case  20: // y1
                                    double_3D_1.y = double_value;
                                    break;
                        case  30: // z1
                                    double_3D_1.z = double_value;
                                    break;
                        case  11: // x2
                                    double_3D_2.x = double_value;
                                    break;
                        case  21: // y2
                                    double_3D_2.y = double_value;
                                    break;
                        case  31: // z2
                                    double_3D_2.z = double_value;
                                    break;
                        case  62: // dxf_color_index
                                    color = p_space->palette_picker(LASERBOY_DXF)[(u_char)double_value];
                                    if(color.is_black())
                                        color = p_space->palette_picker(LASERBOY_DXF)[p_space->palette_picker(LASERBOY_DXF).white];
                                    break;
                        case 420: // true color
                                    color = (u_int)double_value;
                                    if(color.is_black())
                                        color = (u_int)0X00ffffff;
                                    break;
                    }
                }
                push_back(LaserBoy_real_vertex(double_3D_1, color).blank());
                push_back(LaserBoy_real_vertex(double_3D_2, color).unblank());
            }
            //----------------------------------------------------------------
            else if(    entity_string == "CIRCLE"
                     || entity_string == "ARC"
                   )
            {
                u_int a;
                while(get_dxf_pair(in, group_code, entity_string))
                {
                    sscanf(entity_string.c_str(), "%lf", &double_value);
                    switch(group_code)
                    {
                        case  10: // x1
                                    double_3D_1.x = double_value;
                                    break;
                        case  20: // y1
                                    double_3D_1.y = double_value;
                                    break;
                        case  30: // z1
                                    double_3D_1.z = double_value;
                                    break;
                        case  40: // radius
                                    radius = double_value;
                                    break;
                        case  50: // start angle
                                    arc_start = double_value;
                                    break;
                        case  51: // end angle
                                    arc_end = double_value;
                                    break;
                        case  62: // dxf_color_index
                                    color = p_space->palette_picker(LASERBOY_DXF)[(u_char)double_value];
                                    if(color.is_black())
                                        color = p_space->palette_picker(LASERBOY_DXF)[p_space->palette_picker(LASERBOY_DXF).white];
                                    break;
                        case 420: // true color
                                    color = (u_int)double_value;
                                    if(color.is_black())
                                        color = (u_int)0X00ffffff;
                                    break;
                    }
                }

                if(arc_start >= arc_end)
                    arc_end += 360.0;
                arc_angle  = arc_end - arc_start;
                arc_vertices = (int)ceil(arc_angle / p_space->rendered_arc_angle);
                arc_step   = arc_angle / arc_vertices;
                arc_start *= one_degree;
                arc_end   *= one_degree;
                arc_step  *= one_degree;

                double_3D_2.x = radius * cos(arc_start);
                double_3D_2.y = radius * sin(arc_start);
                double_3D_2.z = double_3D_1.z;

                push_back(LaserBoy_real_vertex(double_3D_1 + double_3D_2, color).blank());

                for(a = 1; a <= arc_vertices; a++)
                {
                    double_3D_2.x = radius * cos(a * arc_step + arc_start);
                    double_3D_2.y = radius * sin(a * arc_step + arc_start);
                    double_3D_2.z = double_3D_1.z;

                    push_back(LaserBoy_real_vertex(double_3D_1 + double_3D_2, color).unblank());
                }
            }
            //----------------------------------------------------------------
            else if(entity_string == "ELLIPSE")
            {
                u_int a,
                      vertex_index;
                while(get_dxf_pair(in, group_code, entity_string))
                {
                    sscanf(entity_string.c_str(), "%lf", &double_value);
                    switch(group_code)
                    {
                        case  10: // x1
                                    double_3D_1.x = double_value;
                                    break;
                        case  20: // y1
                                    double_3D_1.y = double_value;
                                    break;
                        case  30: // z1
                                    double_3D_1.z = double_value;
                                    break;
                        case  11: // x2
                                    double_3D_2.x = double_value;
                                    break;
                        case  21: // y2
                                    double_3D_2.y = double_value;
                                    break;
                        case  31: // z2
                                    double_3D_2.z = double_value;
                                    break;
                        case  40: // ratio between major and minor axis
                                    radius = double_value;
                                    break;
                        case  41: // start angle
                                    arc_start = double_value;
                                    break;
                        case  42: // end angle
                                    arc_end = double_value;
                                    break;
                        case  62: // dxf_color_index
                                    color = p_space->palette_picker(LASERBOY_DXF)[(u_char)double_value];
                                    if(color.is_black())
                                        color = p_space->palette_picker(LASERBOY_DXF)[p_space->palette_picker(LASERBOY_DXF).white];
                                    break;
                        case 420: // true color
                                    color = (u_int)double_value;
                                    if(color.is_black())
                                        color = (u_int)0X00ffffff;
                                    break;
                    }
                }
                if(arc_start >= arc_end) arc_end += two_pi;
                arc_angle  = arc_end - arc_start;
                arc_vertices = (int)ceil(arc_angle / (p_space->rendered_arc_angle * one_degree));
                arc_step   = arc_angle / arc_vertices;
                double major_axis = double_3D_2.magnitude(),
                      minor_axis = major_axis * radius;
                double_3D_3   = 0.0;
                double_3D_3.z = -double_3D_2.direction().z;
                real_vertices.clear();
                double_3D_4.x = -major_axis * cos(arc_start);
                double_3D_4.y = -minor_axis * sin(arc_start);
                double_3D_4.z = double_3D_1.z;
                real_vertices.push_back(LaserBoy_real_vertex(double_3D_4, color).blank());
                for(a = 1; a <= arc_vertices; a++)
                {
                    double_3D_4.x = -major_axis * cos(a * arc_step + arc_start);
                    double_3D_4.y = -minor_axis * sin(a * arc_step + arc_start);
                    double_3D_4.z = double_3D_1.z;

                    real_vertices.push_back(LaserBoy_real_vertex(double_3D_4, color).unblank());
                }
                if(double_3D_3 != 0.0)
                    for(vertex_index = 0; vertex_index < real_vertices.size(); vertex_index++)
                        real_vertices[vertex_index] = rotate_vertex(real_vertices[vertex_index], double_3D_3);
                for(vertex_index = 0; vertex_index < real_vertices.size(); vertex_index++)
                    push_back(real_vertices[vertex_index] + double_3D_1);
            }
            //----------------------------------------------------------------
            else if(entity_string == "POLYLINE")
            {
                while(get_dxf_pair(in, group_code, entity_string))
                {
                    sscanf(entity_string.c_str(), "%lf", &double_value);
                    switch(group_code)
                    {
                        case  62: // dxf_color_index
                                    dxf_color_index = (u_char)double_value;
                                    prev_dxf_color_index = dxf_color_index;
                                    prev_color = p_space->palette_picker(LASERBOY_DXF)[dxf_color_index];
                                    if(prev_color.is_black())
                                        prev_color = p_space->palette_picker(LASERBOY_DXF)[p_space->palette_picker(LASERBOY_DXF).white];
                                    color = p_space->palette_picker(LASERBOY_DXF)[dxf_color_index];
                                    if(color.is_black())
                                        color = p_space->palette_picker(LASERBOY_DXF)[p_space->palette_picker(LASERBOY_DXF).white];
                                    break;
                        case 420: // true color
                                    color = (u_int)double_value;
                                    if(color.is_black())
                                        color = (u_int)0X00ffffff;
                                    prev_color = color;
                                    break;
                        case  70:
                                    if(((int)double_value) & 0x01)
                                        closed_polyline = true;
                                    break;
                    }
                }
                while(entity_string == "VERTEX")
                {
                    while(get_dxf_pair(in, group_code, entity_string))
                    {
                        sscanf(entity_string.c_str(), "%lf", &double_value);
                        switch(group_code)
                        {
                            case  10: // x1
                                        double_3D_1.x = double_value;
                                        break;
                            case  20: // y1
                                        double_3D_1.y = double_value;
                                        break;
                            case  30: // z1
                                        double_3D_1.z = double_value;
                                        break;
                            case  62: // dxf_color_index
                                        prev_dxf_color_index = dxf_color_index;
                                        dxf_color_index = (u_char)double_value;
                                        prev_color = p_space->palette_picker(LASERBOY_DXF)[prev_dxf_color_index];
                                        if(prev_color.is_black())
                                            prev_color = p_space->palette_picker(LASERBOY_DXF)[p_space->palette_picker(LASERBOY_DXF).white];
                                        color = p_space->palette_picker(LASERBOY_DXF)[(u_char)double_value];
                                        if(color.is_black())
                                            color = p_space->palette_picker(LASERBOY_DXF)[p_space->palette_picker(LASERBOY_DXF).white];
                                        break;
                            case 420: // true color
                                        prev_color = color;
                                        color = (u_int)double_value;
                                        if(color.is_black())
                                            color = (u_int)0X00ffffff;
                                        break;
                        }
                    }
                    if(first_segment_vertex_index == -1)
                    {
                        push_back(LaserBoy_real_vertex(double_3D_1, color).blank());
                        first_segment_vertex_index = (int)size() - 1;
                    }
                    else
                        push_back(LaserBoy_real_vertex(double_3D_1, prev_color).unblank());
                } // end while(entity_string == "VERTEX")
                if(closed_polyline)
                    push_back(LaserBoy_real_vertex((LaserBoy_3D_double)(at(first_segment_vertex_index)), back().as_LaserBoy_color()).unblank());
            }
            //----------------------------------------------------------------
            else if(entity_string == "LWPOLYLINE")
            {
                while(get_dxf_pair(in, group_code, entity_string))
                {
                    sscanf(entity_string.c_str(), "%lf", &double_value);
                    switch(group_code)
                    {
                        case  10: // x
                                    vertex_x.push_back(double_value);
                                    break;
                        case  20: // y
                                    vertex_y.push_back(double_value);
                                    break;
                        case  62: // dxf_color_index
                                    color = p_space->palette_picker(LASERBOY_DXF)[(u_char)double_value];
                                    if(color.is_black())
                                        color = p_space->palette_picker(LASERBOY_DXF)[p_space->palette_picker(LASERBOY_DXF).white];
                                    break;
                        case 420: // true color
                                    color = (u_int)double_value;
                                    if(color.is_black())
                                        color = (u_int)0X00ffffff;
                                    break;
                        case  70: // is closed polyline
                                    group_70_flag = (int)double_value;
                                    break;
                        case  90: // number of verticies
                                    arc_vertices = (int)double_value;
                                    break;
                    }
                }
                if(    (arc_vertices > 0)
                    && (arc_vertices == vertex_x.size())
                    && (arc_vertices == vertex_y.size())
                  )
                {
                    double_3D_1.x = vertex_x.front();
                    double_3D_1.y = vertex_y.front();
                    double_3D_1.z = 0;
                    push_back(LaserBoy_real_vertex(double_3D_1, color).blank());
                    for(u_int a = 0; a < arc_vertices; a++)
                    {
                        double_3D_1.x = vertex_x[a];
                        double_3D_1.y = vertex_y[a];
                        push_back(LaserBoy_real_vertex(double_3D_1, color).unblank());
                    }
                    if(group_70_flag & 0x01) // closed polyline
                    {
                        double_3D_1.x = vertex_x.front();
                        double_3D_1.y = vertex_y.front();
                        push_back(LaserBoy_real_vertex(double_3D_1, color).unblank());
                    }
                }
            }
            //----------------------------------------------------------------
            else if(entity_string == "3DFACE")                
            {
                u_int vertex_count = 0;
                vertex_x.resize(10);
                vertex_y.resize(10);
                vertex_z.resize(10);
                while(get_dxf_pair(in, group_code, entity_string))
                {
                    sscanf(entity_string.c_str(), "%lf", &double_value);                    
                    if     (group_code < 20 && group_code >  9)
                    {
                        vertex_x[group_code - 10] = double_value;
                        vertex_count++;
                    }
                    else if(group_code < 30 && group_code > 19)
                        vertex_y[group_code - 20] = double_value;
                    else if(group_code < 40 && group_code > 29)
                        vertex_z[group_code - 30] = double_value;
                }
                double_3D_1.x = vertex_x.front();
                double_3D_1.y = vertex_y.front();
                double_3D_1.z = vertex_z.front();
                push_back(LaserBoy_real_vertex(double_3D_1, color).blank());
                for(u_int a = 0; a < vertex_count; a++)
                {
                    double_3D_1.x = vertex_x[a];
                    double_3D_1.y = vertex_y[a];
                    double_3D_1.z = vertex_z[a];
                    push_back(LaserBoy_real_vertex(double_3D_1, color).unblank());
                }
                double_3D_1.x = vertex_x.front();
                double_3D_1.y = vertex_y.front();
                double_3D_1.z = vertex_z.front();
                push_back(LaserBoy_real_vertex(double_3D_1, color).unblank());
            }
            //----------------------------------------------------------------
            else if(entity_string == "TEXT")
            {
                int                text_index,
                                   vertex_index;
                long int           bytes_skipped;
                LaserBoy_frame_set font_frames(p_space);
                double_3D_1 = 0.0;
                double_3D_3 = 0.0;
                while(get_dxf_pair(in, group_code, entity_string))
                {
                    sscanf(entity_string.c_str(), "%lf", &double_value);
                    switch(group_code)
                    {
                        case   1: // the text
                                    text = entity_string;
                                    break;
                        case   7: // the font
                                    font = entity_string;
                                    break;
                        case  10: // x1
                                    double_3D_1.x = double_value;
                                    break;
                        case  20: // y1
                                    double_3D_1.y = double_value;
                                    break;
                        case  30: // z1
                                    double_3D_1.z = double_value;
                                    break;
                        case  40: // text height
                                    radius = double_value;
                                    break;
                        case  50: // flat rotation angle
                                    double_3D_3.z = double_value * one_degree;
                                    break;
                        case  51: // oblique rotation angle
                                    double_3D_3.y = double_value * one_degree;
                                    break;
                        case  62: // dxf_color_index
                                    color = p_space->palette_picker(LASERBOY_DXF)[(u_char)double_value];
                                    if(color.is_black())
                                        color = p_space->palette_picker(LASERBOY_DXF)[p_space->palette_picker(LASERBOY_DXF).white];
                                    break;
                        case 420: // true color
                                    color = (u_int)double_value;
                                    if(color.is_black())
                                        color = (u_int)0X00ffffff;
                                    break;
                        case  71: // flipped or mirrored
                                    group_71_flag = (int)double_value;
                                    break;
                        case  72: // horizontal justification
                                    group_72_flag = (int)double_value;
                                    break;
                        case  73: // vertical justification
                                    group_73_flag = (int)double_value;
                                    break;
                    }
                }
                //------------------------------------------------------------
                if(font == "ARIAL")
                    font_frames.from_ild_file(LASERBOY_ILD_SHARE + string("fonts/") + "arial.ild", bytes_skipped, counter);
                else if(font == "COMIC_SANS_MS")
                    font_frames.from_ild_file(LASERBOY_ILD_SHARE + string("fonts/") + "comic_sans.ild", bytes_skipped, counter);
                else if(font == "COURIER_NEW")
                    font_frames.from_ild_file(LASERBOY_ILD_SHARE + string("fonts/") + "courier_new.ild", bytes_skipped, counter);
                else if(font == "LUCIDA_CONSOLE")
                    font_frames.from_ild_file(LASERBOY_ILD_SHARE + string("fonts/") + "lucida.ild", bytes_skipped, counter);
                else if(font == "IMPACT")
                    font_frames.from_ild_file(LASERBOY_ILD_SHARE + string("fonts/") + "impact.ild", bytes_skipped, counter);
                else if(font == "TIMES_NEW_ROMAN")
                    font_frames.from_ild_file(LASERBOY_ILD_SHARE + string("fonts/") + "times_new_roman.ild", bytes_skipped, counter);
                else
                    font_frames.from_ild_file(LASERBOY_ILD_SHARE + string("fonts/") + "narrow_vector.ild", bytes_skipped, counter);
                if(    font_frames.frame_set_error != LASERBOY_OK
                    || font_frames.number_of_frames() < (('~' - '!') + 1)
                  )
                    real_segment_error = LASERBOY_BAD_FONT_FILE;
                else
                {
                    real_vertices.clear();
                    for(text_index = 0; text_index < (int)text.size(); text_index++)
                    {
                        if(text[text_index] >= '!' && text[text_index] <= '~')
                        {
                            for(vertex_index = 0; vertex_index < (int)font_frames[text[text_index] - '!'].size(); vertex_index++)
                            {
                                double_3D_2 = font_frames[text[text_index] - '!'].at(vertex_index);
                                //------------------------------------------------
                                double_3D_2.x = (   (   double_3D_2.x
                                                      / (   p_space->rendered_mono_font_space
                                                          * LASERBOY_MAX_SHORT
                                                        )
                                                      + (double)(text_index)
                                                    )
                                                ) * radius;
                                //------------------------------------------------
                                double_3D_2.y = (   (   double_3D_2.y
                                                      / (   p_space->rendered_mono_font_space
                                                          * LASERBOY_MAX_SHORT
                                                        )
                                                    )
                                                ) * radius;
                                //------------------------------------------------
                                real_vertices.push_back(   (font_frames[text[text_index] - '!'].at(vertex_index).is_blank())
                                                         ? (LaserBoy_real_vertex(double_3D_2, color).blank())
                                                         : (   (font_frames[text[text_index] - '!'].at(vertex_index).is_black(p_space->black_level))
                                                             ? (LaserBoy_real_vertex(double_3D_2, LaserBoy_color(0,0,0)).unblank())
                                                             : (LaserBoy_real_vertex(double_3D_2, color).unblank())
                                                           )
                                                       );
                            }
                        }
                    }
                    if(group_71_flag & 2) // mirrored
                        real_vertices.flip(0);
                    if(group_71_flag & 4) // flipped
                        real_vertices.flip(1);
                    if(double_3D_3 != 0.0)
                        for(vertex_index = 0; vertex_index < (int)real_vertices.size(); vertex_index++)
                            real_vertices[vertex_index] = rotate_vertex(real_vertices[vertex_index], double_3D_3);
                    for(vertex_index = 0; vertex_index < (int)real_vertices.size(); vertex_index++)
                        push_back(real_vertices[vertex_index] + double_3D_1);
                }
            } // end else if(entity_string == "TEXT")
            //----------------------------------------------------------------
            else if(entity_string == "ENDSEC")
                break;
            //----------------------------------------------------------------
            else
                get_dxf_pair(in, group_code, entity_string);
        } // end while(in.good())
    } // end if(in.good())
    //------------------------------------------------------------------------
    else
    {
        real_segment_error = LASERBOY_EOF;
    }
    //------------------------------------------------------------------------
    return real_segment_error;
}

//############################################################################
LaserBoy_Error_Code LaserBoy_real_segment::from_ifstream_txt(std::ifstream& in,
                                                             const u_int&   group_type,
                                                             int&           line_number
                                                            )
{
    LaserBoy_real_vertex real_vertex;
    //------------------------------------------------------------------------
    if(    group_type == LASERBOY_3D_FRAME_PALETTE
        || group_type == LASERBOY_2D_FRAME_PALETTE
      )
        palette_index = p_space->palette_index;
    else
        palette_index = LASERBOY_TRUE_COLOR;
    //------------------------------------------------------------------------
    clear();
    push_back(LaserBoy_real_vertex());
    push_back(LaserBoy_real_vertex());
    //------------------------------------------------------------------------
    if(in.good())
    {
        real_segment_error = LASERBOY_OK;
        while(real_vertex.from_ifstream_txt(in, group_type, line_number))
            push_back(real_vertex);
        sync_rgb_and_palette();
    }
    else
        real_segment_error = LASERBOY_EOF;
    //------------------------------------------------------------------------
    return real_segment_error;
}

//############################################################################
void LaserBoy_real_segment::normalize(bool ignore_origin) // if(p_space->maintain_real_origin)
{
    if(size() > 2)
    {
        int                 _x,
                            _y,
                            _z;
        u_int               i,
                            start       = 0;
        double              real_size   =  0.0,
                            real_scale  =  1.0;
        LaserBoy_3D_double  real_min    =  DBL_MAX,
                            real_max    = -DBL_MAX,
                            real_offset =  0.0;
        if(ignore_origin)
            start = 2;
        for(i = start; i < size(); i++) // ignore the origin vector
        {
            if(at(i).x > real_max.x)    real_max.x = at(i).x;
            if(at(i).x < real_min.x)    real_min.x = at(i).x;
            if(at(i).y > real_max.y)    real_max.y = at(i).y;
            if(at(i).y < real_min.y)    real_min.y = at(i).y;
            if(at(i).z > real_max.z)    real_max.z = at(i).z;
            if(at(i).z < real_min.z)    real_min.z = at(i).z;
        }
        if(p_space->maintain_real_origin)
        {
            if(fabs(real_max.x) > real_size)    real_size = fabs(real_max.x);
            if(fabs(real_min.x) > real_size)    real_size = fabs(real_min.x);
            if(fabs(real_max.y) > real_size)    real_size = fabs(real_max.y);
            if(fabs(real_min.y) > real_size)    real_size = fabs(real_min.y);
            if(fabs(real_max.z) > real_size)    real_size = fabs(real_max.z);
            if(fabs(real_min.z) > real_size)    real_size = fabs(real_min.z);
            real_size *= 2;
        }
        else // find the new center of the universe
        {
            real_offset.x = (real_max.x - ((real_max.x - real_min.x) / 2));
            real_offset.y = (real_max.y - ((real_max.y - real_min.y) / 2));
            real_offset.z = (real_max.z - ((real_max.z - real_min.z) / 2));
            if(fabs(real_max.x - real_min.x) > real_size)
                real_size = fabs(real_max.x - real_min.x);
            if(fabs(real_max.y - real_min.y) > real_size)
                real_size = fabs(real_max.y - real_min.y);
            if(fabs(real_max.z - real_min.z) > real_size)
                real_size = fabs(real_max.z - real_min.z);
        }
        real_scale = 65535.0 / real_size;
        //----------------------------------------------------------------
        for(i = start; i < size(); i++) // ignore the origin vector
        {
            _x = (int)round((at(i).x - real_offset.x) * real_scale);
            _y = (int)round((at(i).y - real_offset.y) * real_scale);
            _z = (int)round((at(i).z - real_offset.z) * real_scale);
            if(_x >  32767) _x =  32767;    if(_x < -32767) _x = -32767;
            if(_y >  32767) _y =  32767;    if(_y < -32767) _y = -32767;
            if(_z >  32767) _z =  32767;    if(_z < -32767) _z = -32767;
            at(i).x = (double)(_x);
            at(i).y = (double)(_y);
            at(i).z = (double)(_z);
        }
    }
    //--------------------------------------------------------------------
    return;
}

//############################################################################
void LaserBoy_real_segment::normalize_vectors(bool ignore_origin)
{
    if(size() > 2)
    {
        int                 _x,
                            _y,
                            _z;
        u_int               i,
                            start       =  0;
        double              real_size   =  0.0,
                            real_scale  =  1.0;
        LaserBoy_3D_double  real_min    =  DBL_MAX,
                            real_max    = -DBL_MAX,
                            real_offset =  0.0;
        if(ignore_origin)
            start = 2;
        for(i = start; i < size(); i++) // ignore the origin vector
        {
            if(at(i).x > real_max.x)    real_max.x = at(i).x;
            if(at(i).x < real_min.x)    real_min.x = at(i).x;
            if(at(i).y > real_max.y)    real_max.y = at(i).y;
            if(at(i).y < real_min.y)    real_min.y = at(i).y;
            if(at(i).z > real_max.z)    real_max.z = at(i).z;
            if(at(i).z < real_min.z)    real_min.z = at(i).z;
        }
        real_offset.x = (real_max.x - ((real_max.x - real_min.x) / 2));
        real_offset.y = (real_max.y - ((real_max.y - real_min.y) / 2));
        real_offset.z = (real_max.z - ((real_max.z - real_min.z) / 2));
        if(fabs(real_max.x - real_min.x) > real_size)
            real_size = fabs(real_max.x - real_min.x);
        if(fabs(real_max.y - real_min.y) > real_size)
            real_size = fabs(real_max.y - real_min.y);
        if(fabs(real_max.z - real_min.z) > real_size)
            real_size = fabs(real_max.z - real_min.z);
        real_scale = 65535.0 / real_size;
        //----------------------------------------------------------------
        for(i = start; i < size(); i++) // ignore the origin vector
        {
            _x = (int)round((at(i).x - real_offset.x) * real_scale);
            _y = (int)round((at(i).y - real_offset.y) * real_scale);
            _z = (int)round((at(i).z - real_offset.z) * real_scale);
            if(_x >  32767) _x =  32767;    if(_x < -32767) _x = -32767;
            if(_y >  32767) _y =  32767;    if(_y < -32767) _y = -32767;
            if(_z >  32767) _z =  32767;    if(_z < -32767) _z = -32767;
            at(i).x = (double)(_x);
            at(i).y = (double)(_y);
            at(i).z = (double)(_z);
        }
    }
    //--------------------------------------------------------------------
    return;
}

//############################################################################
void LaserBoy_real_segment::normalize_vectors_with_origin(bool ignore_origin)
{
    if(size() > 2)
    {
        int                 _x,
                            _y,
                            _z;
        u_int               i,
                            start       =  0;
        double              real_size   =  0.0,
                            real_scale  =  1.0;
        LaserBoy_3D_double  real_min    =  DBL_MAX,
                            real_max    = -DBL_MAX;
        if(ignore_origin)
            start = 2;
        for(i = start; i < size(); i++) // ignore the origin vector
        {
            if(at(i).x > real_max.x)    real_max.x = at(i).x;
            if(at(i).x < real_min.x)    real_min.x = at(i).x;
            if(at(i).y > real_max.y)    real_max.y = at(i).y;
            if(at(i).y < real_min.y)    real_min.y = at(i).y;
            if(at(i).z > real_max.z)    real_max.z = at(i).z;
            if(at(i).z < real_min.z)    real_min.z = at(i).z;
        }
        if(fabs(real_max.x) > real_size)    real_size = fabs(real_max.x);
        if(fabs(real_min.x) > real_size)    real_size = fabs(real_min.x);
        if(fabs(real_max.y) > real_size)    real_size = fabs(real_max.y);
        if(fabs(real_min.y) > real_size)    real_size = fabs(real_min.y);
        if(fabs(real_max.z) > real_size)    real_size = fabs(real_max.z);
        if(fabs(real_min.z) > real_size)    real_size = fabs(real_min.z);
        real_size *= 2;
        real_scale = 65535.0 / real_size;
        //----------------------------------------------------------------
        for(i = start; i < size(); i++) // ignore the origin vector
        {
            _x = (int)round((at(i).x) * real_scale);
            _y = (int)round((at(i).y) * real_scale);
            _z = (int)round((at(i).z) * real_scale);
            if(_x >  32767) _x =  32767;    if(_x < -32767) _x = -32767;
            if(_y >  32767) _y =  32767;    if(_y < -32767) _y = -32767;
            if(_z >  32767) _z =  32767;    if(_z < -32767) _z = -32767;
            at(i).x = (double)(_x);
            at(i).y = (double)(_y);
            at(i).z = (double)(_z);
        }
    }
    //--------------------------------------------------------------------
    return;
}

//############################################################################
LaserBoy_real_segment& LaserBoy_real_segment::clip(const LaserBoy_3D_double& max,
                                                   const LaserBoy_3D_double& min,
                                                   const double&             granularity
                                                  )
{
    if(size() > 1)
    {
        LaserBoy_real_segment clipped(p_space);
        LaserBoy_3D_double    c1,
                              c2;
        LaserBoy_real_vertex  blank;
        //----------------------------------------------------------------
        clipped.palette_index = palette_index;
        reduce_blank_vectors();
        if(LaserBoy_bounds_check(at(0), max, min))
        {
            switch(clip_vector(at(0), at(1), max, min, granularity, c1, c2))
            {
                case no_line:
                    clipped.push_back(blank);
                    break;                 
                case p1_in_p2_in: // not possible
                case p1_in_p2_out:
                    clipped.push_back(at(0));
                    break;
                case p1_out_p2_in:
                case p1_out_p2_out:
                    clipped.push_back(at(0));
                    clipped.back() = c1;
                    break;
                case bounds_error:
                    normalize_vectors_with_origin();
                    return *this;
            }
        }
        else
            clipped.push_back(at(0));
        for(u_int i = 1; i < size(); i++)
        {
            if(LaserBoy_bounds_check(at(i - 1), max, min) || LaserBoy_bounds_check(at(i), max, min))
            {
                switch(clip_vector(at(i - 1), at(i), max, min, granularity, c1, c2))
                {
                    case no_line:
                        clipped.push_back(blank);
                        break;                 
                    case p1_in_p2_in: // not possible
                        clipped.push_back(at(i));
                        break;
                    case p1_in_p2_out:
                        clipped.push_back(at(i));
                        clipped.back() = c2;
                        break;
                    case p1_out_p2_in:
                        clipped.push_back(blank);
                        clipped.back() = c1;
                        clipped.push_back(at(i));
                        break;
                    case p1_out_p2_out:
                        clipped.push_back(blank);
                        if(at(i).is_lit())
                        {
                            clipped.back() = c1;
                            clipped.push_back(at(i));
                            clipped.back() = c2;
                        }
                        break;
                    case bounds_error:
                        normalize_vectors_with_origin();
                        return *this;
                }
            } // if(LaserBoy_bounds_check(at(i - 1), max, min) || LaserBoy_bounds_check(at(i), max, min))
            else // at(i - 1) and at(i) are both in bounds
                clipped.push_back(at(i));
        } // end for(u_int i = 1; i < size(); i++)
        clipped.reduce_blank_vectors();
        if(clipped.size() > 1)
            *this = clipped;
        else
            clear();
        return *this;
    } // end if(size() > 1)
    clear();
    return *this;
}

//############################################################################
LaserBoy_real_segment& LaserBoy_real_segment::clip()
{
    return clip(LaserBoy_3D_double( 32767.0,  32767.0,  32767.0),
                LaserBoy_3D_double(-32767.0, -32767.0, -32767.0),
                65536.0
               );
}

//############################################################################
LaserBoy_real_segment& LaserBoy_real_segment::clip_around_coordinate(const LaserBoy_3D_double& center,
                                                                     const LaserBoy_3D_double& max,
                                                                     const LaserBoy_3D_double& min,
                                                                     const double&             granularity
                                                                    )
{
    LaserBoy_real_segment rs(*this);
    if(size() > 1)
    {
        rs.clip(max + center, min + center, granularity);
        rs -= center;
        *this = rs;
    }
    return *this;
}

//############################################################################
LaserBoy_real_segment& LaserBoy_real_segment::clip_around_coordinate(const LaserBoy_3D_double& center,
                                                                     const double              range,
                                                                     const double&             granularity
                                                                    )
{
    LaserBoy_real_segment rs(*this);
    if(size() > 1)
    {
        rs.clip(LaserBoy_3D_double( range + center.x,  range + center.y,  range + center.z),
                LaserBoy_3D_double(-range + center.x, -range + center.y, -range + center.z),
                granularity
               );
        rs -= center;
        *this = rs;
    }
    return *this;
}

//############################################################################
//////////////////////////////////////////////////////////////////////////////
//############################################################################
LaserBoy_real_segment_set::LaserBoy_real_segment_set(LaserBoy_space*   ps,
                                                     const int         n,
                                                     const double      radius,
                                                     const double      pedals_numerator,
                                                     const double      pedals_denominator,
                                                     const double      _radius,
                                                     const double      _pedals_numerator,
                                                     const double      _pedals_denominator,
                                                     const double      start,
                                                     const double      duration,
                                                     const int         frames
                                                    )  // uddddddddu animated rhodonea (uddddd rhodonea)
                           : p_space(ps)
{
    double ratio = 1.0;
    p_space->p_GUI->display_state("rendering epitrochoid or epicycloid frames");
    for(int frame = 0; frame < frames; frame++)
    {
        ratio = (double)frame / frames;
        push_back(LaserBoy_real_segment(p_space,                                        
                                        n,
                                        radius             * (1.0 - ratio) +             _radius * ratio,
                                        pedals_numerator   * (1.0 - ratio) +   _pedals_numerator * ratio,
                                        pedals_denominator * (1.0 - ratio) + _pedals_denominator * ratio,
                                        start,
                                        duration
                                       )
                 );
        p_space->p_GUI->display_progress(frames - frame);
    }
}

//############################################################################
LaserBoy_real_segment_set::LaserBoy_real_segment_set(LaserBoy_space*   ps,
                                                     const double      center_radius,
                                                     const double      roller_radius,
                                                     const double      roller_offset,
                                                     const double      _center_radius,
                                                     const double      _roller_radius,
                                                     const double      _roller_offset,
                                                     const int         n,
                                                     const double      start,
                                                     const double      duration,
                                                     const int         frames
                                                    ) // ddddudu animated epitrochoid epicycloid (dddud epitrochoid epicycloid)
                           : p_space(ps)
{
    double ratio = 1.0;
    p_space->p_GUI->display_state("rendering epicycloid or epitrochoid frames");
    for(int frame = 0; frame < frames; frame++)
    {
        ratio = (double)frame / frames;
        push_back(LaserBoy_real_segment(p_space,                                        
                                        center_radius * (1.0 - ratio) + _center_radius * ratio,
                                        roller_radius * (1.0 - ratio) + _roller_radius * ratio,
                                        roller_offset * (1.0 - ratio) + _roller_offset * ratio,
                                        n,
                                        start,
                                        duration
                                       )
                 );
        p_space->p_GUI->display_progress(frames - frame);
    }
}

//############################################################################
LaserBoy_real_segment_set::LaserBoy_real_segment_set(LaserBoy_space*  ps,
                                                     const double      start,
                                                     const double      duration,
                                                     const int         frames,
                                                     const double      center_radius,
                                                     const double      roller_radius,
                                                     const double      roller_offset,
                                                     const double      _center_radius,
                                                     const double      _roller_radius,
                                                     const double      _roller_offset,
                                                     const int         n
                                                    ) // duddddddu animated hypotrochoid hypocycloid (ddddu hypotrochoid hypocycloid)
                           : p_space(ps)
{
    double ratio = 1.0;
    p_space->p_GUI->display_state("rendering hypocycloid or hypotrochoid frames");
    for(int frame = 0; frame < frames; frame++)
    {
        ratio = (double)frame / frames;
        push_back(LaserBoy_real_segment(p_space,                                        
                                        start,
                                        duration,
                                        center_radius * (1.0 - ratio) + _center_radius * ratio,
                                        roller_radius * (1.0 - ratio) + _roller_radius * ratio,
                                        roller_offset * (1.0 - ratio) + _roller_offset * ratio,
                                        n
                                       )
                 );
        p_space->p_GUI->display_progress(frames - frame);
    }
}

//############################################################################
LaserBoy_real_segment_set::LaserBoy_real_segment_set(LaserBoy_space*   ps,
                                                     LaserBoy_pendulum P1,
                                                     LaserBoy_pendulum _P1,
                                                     const int         n,
                                                     const double      duration,                              
                                                     const int         frames
                                                    ) // animated pendulums constructor
                           : p_space(ps)
{
    p_space->p_GUI->display_state("rendering harmonograph frames");
    for(int frame = 0; frame < frames; frame++)
    {
        push_back(LaserBoy_real_segment(p_space,
                                        P1.blend(_P1, (double)frame / frames),
                                        n,
                                        duration
                                       )
                 );
        p_space->p_GUI->display_progress(frames - frame);
    }
}

//############################################################################
LaserBoy_real_segment_set::LaserBoy_real_segment_set(LaserBoy_space*   ps,
                                                     const double      duration,                              
                                                     const int         n,
                                                     LaserBoy_pendulum P1,
                                                     LaserBoy_pendulum P2,
                                                     LaserBoy_pendulum _P1,
                                                     LaserBoy_pendulum _P2,
                                                     const int         frames
                                                    ) // animated pendulums_sum constructor
                           : p_space(ps)
{
    p_space->p_GUI->display_state("rendering harmonograph frames");
    for(int frame = 0; frame < frames; frame++)
    {
        push_back(LaserBoy_real_segment(p_space,
                                        duration,
                                        n,
                                        P1.blend(_P1, (double)frame / frames),
                                        P2.blend(_P2, (double)frame / frames)
                                       )
                 );
        p_space->p_GUI->display_progress(frames - frame);
    }
}

//############################################################################
LaserBoy_real_segment_set::LaserBoy_real_segment_set(LaserBoy_space*   ps,
                                                     LaserBoy_pendulum P1,
                                                     LaserBoy_pendulum P2,
                                                     LaserBoy_pendulum _P1,
                                                     LaserBoy_pendulum _P2,
                                                     const int         n,
                                                     const double      duration,                              
                                                     const int         frames
                                                    ) // animated pendulum_xy constructor
                           : p_space(ps)
{
    p_space->p_GUI->display_state("rendering harmonograph frames");
    for(int frame = 0; frame < frames; frame++)
    {
        push_back(LaserBoy_real_segment(p_space,
                                        P1.blend(_P1, (double)frame / frames),
                                        P2.blend(_P2, (double)frame / frames),
                                        n,
                                        duration
                                       )
                 );
        p_space->p_GUI->display_progress(frames - frame);
    }
}

//############################################################################
LaserBoy_real_segment_set::LaserBoy_real_segment_set(LaserBoy_space*   ps,
                                                     LaserBoy_pendulum P1,
                                                     LaserBoy_pendulum P2,
                                                     LaserBoy_pendulum P3,
                                                     LaserBoy_pendulum _P1,
                                                     LaserBoy_pendulum _P2,
                                                     LaserBoy_pendulum _P3,
                                                     const int         n,
                                                     const double      duration,                              
                                                     const int         frames
                                                    ) // animated pendulum_xyz constructor
                           : p_space(ps)
{
    p_space->p_GUI->display_state("rendering harmonograph frames");
    for(int frame = 0; frame < frames; frame++)
    {
        push_back(LaserBoy_real_segment(p_space,
                                        P1.blend(_P1, (double)frame / frames),
                                        P2.blend(_P2, (double)frame / frames),
                                        P3.blend(_P3, (double)frame / frames),
                                        n,
                                        duration
                                       )
                 );
        p_space->p_GUI->display_progress(frames - frame);
    }
}

//############################################################################
LaserBoy_real_segment_set::LaserBoy_real_segment_set(LaserBoy_space*   ps,
                                                     LaserBoy_pendulum P1,
                                                     LaserBoy_pendulum P2,
                                                     LaserBoy_pendulum P3,
                                                     LaserBoy_pendulum P4,
                                                     LaserBoy_pendulum _P1,
                                                     LaserBoy_pendulum _P2,
                                                     LaserBoy_pendulum _P3,
                                                     LaserBoy_pendulum _P4,                              
                                                     const int         n,
                                                     const double      duration,                              
                                                     const int         frames
                                                    ) // animated harmonograph constructor
                           : p_space(ps)
{
    p_space->p_GUI->display_state("rendering harmonograph frames");
    for(int frame = 0; frame < frames; frame++)
    {
        push_back(LaserBoy_real_segment(p_space,
                                        P1.blend(_P1, (double)frame / frames),
                                        P2.blend(_P2, (double)frame / frames),
                                        P3.blend(_P3, (double)frame / frames),
                                        P4.blend(_P4, (double)frame / frames),
                                        n,
                                        duration
                                       )
                 );
        p_space->p_GUI->display_progress(frames - frame);
    }
}

//############################################################################
LaserBoy_real_segment_set::LaserBoy_real_segment_set(LaserBoy_space*   ps,
                                                     LaserBoy_pendulum P1,
                                                     LaserBoy_pendulum P2,
                                                     LaserBoy_pendulum P3,
                                                     LaserBoy_pendulum P4,
                                                     LaserBoy_pendulum P5,
                                                     LaserBoy_pendulum P6,
                                                     LaserBoy_pendulum _P1,
                                                     LaserBoy_pendulum _P2,
                                                     LaserBoy_pendulum _P3,
                                                     LaserBoy_pendulum _P4,                              
                                                     LaserBoy_pendulum _P5,
                                                     LaserBoy_pendulum _P6,
                                                     const int         n,
                                                     const double      duration,
                                                     const int         frames
                                                    ) // animated harmonograph_3D constructor
                           : p_space(ps)
{
    p_space->p_GUI->display_state("rendering harmonograph frames");
    for(int frame = 0; frame < frames; frame++)
    {
        push_back(LaserBoy_real_segment(p_space,
                                        P1.blend(_P1, (double)frame / frames),
                                        P2.blend(_P2, (double)frame / frames),
                                        P3.blend(_P3, (double)frame / frames),
                                        P4.blend(_P4, (double)frame / frames),
                                        P5.blend(_P5, (double)frame / frames),
                                        P6.blend(_P6, (double)frame / frames),
                                        n,
                                        duration
                                       )
                 );
        p_space->p_GUI->display_progress(frames - frame);
    }
}

//############################################################################
LaserBoy_real_segment_set::LaserBoy_real_segment_set(LaserBoy_space*   ps,
                                                     const int         n,
                                                     const double      duration,                              
                                                     LaserBoy_pendulum P1,
                                                     LaserBoy_pendulum P2,
                                                     LaserBoy_pendulum _P1,
                                                     LaserBoy_pendulum _P2,
                                                     const int         frames
                                                    ) // animated amplitude_mods constructor
                           : p_space(ps)
{
    p_space->p_GUI->display_state("rendering amplitude_mods frames");
    for(int frame = 0; frame < frames; frame++)
    {
        push_back(LaserBoy_real_segment(p_space,
                                        n,
                                        duration,
                                        P1.blend(_P1, (double)frame / frames),
                                        P2.blend(_P2, (double)frame / frames)
                                       )
                 );
        p_space->p_GUI->display_progress(frames - frame);
    }
}

//############################################################################
LaserBoy_real_segment_set::LaserBoy_real_segment_set(LaserBoy_space*   ps,
                                                     const int         n,
                                                     const double      duration,                              
                                                     LaserBoy_pendulum P1,
                                                     LaserBoy_pendulum P2,
                                                     LaserBoy_pendulum P3,
                                                     LaserBoy_pendulum P4,
                                                     LaserBoy_pendulum _P1,
                                                     LaserBoy_pendulum _P2,
                                                     LaserBoy_pendulum _P3,
                                                     LaserBoy_pendulum _P4,
                                                     const int         frames
                                                    ) // animated amplitude_mods_xy constructor
                           : p_space(ps)
{
    p_space->p_GUI->display_state("rendering amplitude_mods frames");
    for(int frame = 0; frame < frames; frame++)
    {
        push_back(LaserBoy_real_segment(p_space,
                                        n,
                                        duration,
                                        P1.blend(_P1, (double)frame / frames),
                                        P2.blend(_P2, (double)frame / frames),
                                        P3.blend(_P3, (double)frame / frames),
                                        P4.blend(_P4, (double)frame / frames)
                                       )
                 );
        p_space->p_GUI->display_progress(frames - frame);
    }
}

//############################################################################
LaserBoy_real_segment_set::LaserBoy_real_segment_set(LaserBoy_space*   ps,
                                                     const int         n,
                                                     const double      duration,                              
                                                     LaserBoy_pendulum P1,
                                                     LaserBoy_pendulum P2,
                                                     LaserBoy_pendulum P3,
                                                     LaserBoy_pendulum P4,
                                                     LaserBoy_pendulum P5,
                                                     LaserBoy_pendulum P6,
                                                     LaserBoy_pendulum _P1,
                                                     LaserBoy_pendulum _P2,
                                                     LaserBoy_pendulum _P3,
                                                     LaserBoy_pendulum _P4,
                                                     LaserBoy_pendulum _P5,
                                                     LaserBoy_pendulum _P6,
                                                     const int         frames
                                                    ) // animated amplitude_mods_xyz constructor
                           : p_space(ps)
{
    p_space->p_GUI->display_state("rendering amplitude_mods frames");
    for(int frame = 0; frame < frames; frame++)
    {
        push_back(LaserBoy_real_segment(p_space,
                                        n,
                                        duration,
                                        P1.blend(_P1, (double)frame / frames),
                                        P2.blend(_P2, (double)frame / frames),
                                        P3.blend(_P3, (double)frame / frames),
                                        P4.blend(_P4, (double)frame / frames),
                                        P5.blend(_P5, (double)frame / frames),
                                        P6.blend(_P6, (double)frame / frames)
                                       )
                 );
        p_space->p_GUI->display_progress(frames - frame);
    }
}

//############################################################################
LaserBoy_real_segment_set::LaserBoy_real_segment_set(LaserBoy_space*   ps,
                                                     const int         n,
                                                     LaserBoy_pendulum P1,
                                                     LaserBoy_pendulum P2,
                                                     LaserBoy_pendulum _P1,
                                                     LaserBoy_pendulum _P2,
                                                     const double      duration,                              
                                                     const int         frames
                                                    ) // uppppdu animated frequency_mods constructor (uppd amplitude_mod)
                           : p_space(ps)
{
    p_space->p_GUI->display_state("rendering harmonograph frames");
    for(int frame = 0; frame < frames; frame++)
    {
        push_back(LaserBoy_real_segment(p_space,
                                        n,
                                        P1.blend(_P1, (double)frame / frames),
                                        P2.blend(_P2, (double)frame / frames),
                                        duration
                                       )
                 );
        p_space->p_GUI->display_progress(frames - frame);
    }
}

//############################################################################
LaserBoy_real_segment_set::LaserBoy_real_segment_set(LaserBoy_space*   ps,
                                                     const int         n,
                                                     LaserBoy_pendulum P1,
                                                     LaserBoy_pendulum P2,
                                                     LaserBoy_pendulum P3,
                                                     LaserBoy_pendulum P4,
                                                     LaserBoy_pendulum _P1,
                                                     LaserBoy_pendulum _P2,
                                                     LaserBoy_pendulum _P3,
                                                     LaserBoy_pendulum _P4,
                                                     const double      duration,                              
                                                     const int         frames
                                                    ) // animated frequency_mods_xy constructor
                           : p_space(ps)
{
    p_space->p_GUI->display_state("rendering amplitude_mods frames");
    for(int frame = 0; frame < frames; frame++)
    {
        push_back(LaserBoy_real_segment(p_space,
                                        n,
                                        P1.blend(_P1, (double)frame / frames),
                                        P2.blend(_P2, (double)frame / frames),
                                        P3.blend(_P3, (double)frame / frames),
                                        P4.blend(_P4, (double)frame / frames),
                                        duration
                                       )
                 );
        p_space->p_GUI->display_progress(frames - frame);
    }
}

//############################################################################
LaserBoy_real_segment_set::LaserBoy_real_segment_set(LaserBoy_space*   ps,
                                                     const int         n,
                                                     LaserBoy_pendulum P1,
                                                     LaserBoy_pendulum P2,
                                                     LaserBoy_pendulum P3,
                                                     LaserBoy_pendulum P4,
                                                     LaserBoy_pendulum P5,
                                                     LaserBoy_pendulum P6,
                                                     LaserBoy_pendulum _P1,
                                                     LaserBoy_pendulum _P2,
                                                     LaserBoy_pendulum _P3,
                                                     LaserBoy_pendulum _P4,
                                                     LaserBoy_pendulum _P5,
                                                     LaserBoy_pendulum _P6,
                                                     const double      duration,                              
                                                     const int         frames
                                                    ) // animated frequency_mods_xyz constructor
                           : p_space(ps)
{
    p_space->p_GUI->display_state("rendering amplitude_mods frames");
    for(int frame = 0; frame < frames; frame++)
    {
        push_back(LaserBoy_real_segment(p_space,
                                        n,
                                        P1.blend(_P1, (double)frame / frames),
                                        P2.blend(_P2, (double)frame / frames),
                                        P3.blend(_P3, (double)frame / frames),
                                        P4.blend(_P4, (double)frame / frames),
                                        P5.blend(_P5, (double)frame / frames),
                                        P6.blend(_P6, (double)frame / frames),
                                        duration
                                       )
                 );
        p_space->p_GUI->display_progress(frames - frame);
    }
}

//############################################################################
void LaserBoy_real_segment_set::scale(LaserBoy_3D_double s)
{
    if(size())
        for(u_int i = 0; i < size(); i++)
            at(i).scale(s);
    return;
}

//############################################################################
void LaserBoy_real_segment_set::normalize(bool ignore_origin)
{
    int                 _x,
                        _y,
                        _z;
    u_int               i,
                        j,
                        start       =  0;
    double              real_size   =  0.0,
                        real_scale  =  1.0;
    LaserBoy_3D_double  real_min    =  DBL_MAX,
                        real_max    = -DBL_MAX,
                        real_offset =  0.0;
    if(ignore_origin)
        start = 2;
    for(j = 0; j < size(); j++)
    {
        if(at(j).size() > 2)
        {
            for(i = start; i < at(j).size(); i++) // ignore the origin vector
            {
                if(at(j)[i].x > real_max.x)    real_max.x = at(j)[i].x;
                if(at(j)[i].x < real_min.x)    real_min.x = at(j)[i].x;
                if(at(j)[i].y > real_max.y)    real_max.y = at(j)[i].y;
                if(at(j)[i].y < real_min.y)    real_min.y = at(j)[i].y;
                if(at(j)[i].z > real_max.z)    real_max.z = at(j)[i].z;
                if(at(j)[i].z < real_min.z)    real_min.z = at(j)[i].z;
            }
        }
    }
    if(p_space->maintain_real_origin)
    {
        if(fabs(real_max.x) > real_size)    real_size = fabs(real_max.x);
        if(fabs(real_min.x) > real_size)    real_size = fabs(real_min.x);
        if(fabs(real_max.y) > real_size)    real_size = fabs(real_max.y);
        if(fabs(real_min.y) > real_size)    real_size = fabs(real_min.y);
        if(fabs(real_max.z) > real_size)    real_size = fabs(real_max.z);
        if(fabs(real_min.z) > real_size)    real_size = fabs(real_min.z);
        real_size *= 2;
    }
    else // find the new center of the universe
    {
        real_offset.x = (real_max.x - ((real_max.x - real_min.x) / 2));
        real_offset.y = (real_max.y - ((real_max.y - real_min.y) / 2));
        real_offset.z = (real_max.z - ((real_max.z - real_min.z) / 2));
        if(fabs(real_max.x - real_min.x) > real_size)    real_size = fabs(real_max.x - real_min.x);
        if(fabs(real_max.y - real_min.y) > real_size)    real_size = fabs(real_max.y - real_min.y);
        if(fabs(real_max.z - real_min.z) > real_size)    real_size = fabs(real_max.z - real_min.z);
    }
    if(real_size)
        real_scale = 65535.0 / real_size;
    else
        real_scale = 1.0;
    //------------------------------------------------------------
    for(j = 0; j < size(); j++)
    {
        if(at(j).size() > 2)
        {
            for(i = start; i < at(j).size(); i++) // ignore the origin vector
            {
                _x = (int)round((at(j)[i].x - real_offset.x) * real_scale);
                _y = (int)round((at(j)[i].y - real_offset.y) * real_scale);
                _z = (int)round((at(j)[i].z - real_offset.z) * real_scale);
                if(_x >  32767) _x =  32767;    if(_x < -32767) _x = -32767;
                if(_y >  32767) _y =  32767;    if(_y < -32767) _y = -32767;
                if(_z >  32767) _z =  32767;    if(_z < -32767) _z = -32767;
                at(j)[i].x = (double)(_x);
                at(j)[i].y = (double)(_y);
                at(j)[i].z = (double)(_z);
            }
        }
    }
    //--------------------------------------------------------------------
    return;
}

//############################################################################
void LaserBoy_real_segment_set::normalize_vectors(bool ignore_origin)
{
    int                 _x,
                        _y,
                        _z;
    u_int               i,
                        j,
                        start       =  0;
    double              real_size   =  0.0,
                        real_scale  =  1.0;
    LaserBoy_3D_double  real_min    =  DBL_MAX,
                        real_max    = -DBL_MAX,
                        real_offset =  0.0;
    if(ignore_origin)
        start = 2;
    for(j = 0; j < size(); j++)        
    {
        if(at(j).size() > 2)
        {
            for(i = start; i < at(j).size(); i++) // ignore the origin vector
            {
                if(at(j)[i].x > real_max.x)    real_max.x = at(j)[i].x;
                if(at(j)[i].x < real_min.x)    real_min.x = at(j)[i].x;
                if(at(j)[i].y > real_max.y)    real_max.y = at(j)[i].y;
                if(at(j)[i].y < real_min.y)    real_min.y = at(j)[i].y;
                if(at(j)[i].z > real_max.z)    real_max.z = at(j)[i].z;
                if(at(j)[i].z < real_min.z)    real_min.z = at(j)[i].z;
            }
        }
    }
    real_offset.x = (real_max.x - ((real_max.x - real_min.x) / 2));
    real_offset.y = (real_max.y - ((real_max.y - real_min.y) / 2));
    real_offset.z = (real_max.z - ((real_max.z - real_min.z) / 2));
    if(fabs(real_max.x - real_min.x) > real_size)    real_size = fabs(real_max.x - real_min.x);
    if(fabs(real_max.y - real_min.y) > real_size)    real_size = fabs(real_max.y - real_min.y);
    if(fabs(real_max.z - real_min.z) > real_size)    real_size = fabs(real_max.z - real_min.z);
    if(real_size)
        real_scale = 65535.0 / real_size;
    else
        real_scale = 1.0;
    //------------------------------------------------------------------------
    for(j = 0; j < size(); j++)
    {
        if(at(j).size() > 2)
        {
            for(i = start; i < at(j).size(); i++) // ignore the origin vector
            {
                _x = (int)round((at(j)[i].x - real_offset.x) * real_scale);
                _y = (int)round((at(j)[i].y - real_offset.y) * real_scale);
                _z = (int)round((at(j)[i].z - real_offset.z) * real_scale);
                if(_x >  32767) _x =  32767;    if(_x < -32767) _x = -32767;
                if(_y >  32767) _y =  32767;    if(_y < -32767) _y = -32767;
                if(_z >  32767) _z =  32767;    if(_z < -32767) _z = -32767;
                at(j)[i].x = (double)(_x);
                at(j)[i].y = (double)(_y);
                at(j)[i].z = (double)(_z);
            }
        }
    }
    //------------------------------------------------------------------------
    return;
}

//############################################################################
void LaserBoy_real_segment_set::normalize_vectors_with_origin(bool ignore_origin)
{
    int                 _x,
                        _y,
                        _z;
    u_int               i,
                        j,
                        start       =  0;
    double              real_size   =  0.0,
                        real_scale  =  1.0;
    LaserBoy_3D_double  real_min    =  DBL_MAX,
                        real_max    = -DBL_MAX;
    if(ignore_origin)
        start = 2;
    for(j = 0; j < size(); j++)
    {
        if(at(j).size() > 2)
        {
            for(i = start; i < at(j).size(); i++) // ignore the origin vector
            {
                if(at(j)[i].x > real_max.x)    real_max.x = at(j)[i].x;
                if(at(j)[i].x < real_min.x)    real_min.x = at(j)[i].x;
                if(at(j)[i].y > real_max.y)    real_max.y = at(j)[i].y;
                if(at(j)[i].y < real_min.y)    real_min.y = at(j)[i].y;
                if(at(j)[i].z > real_max.z)    real_max.z = at(j)[i].z;
                if(at(j)[i].z < real_min.z)    real_min.z = at(j)[i].z;
            }
        }
    }
    if(fabs(real_max.x) > real_size)    real_size = fabs(real_max.x);
    if(fabs(real_min.x) > real_size)    real_size = fabs(real_min.x);
    if(fabs(real_max.y) > real_size)    real_size = fabs(real_max.y);
    if(fabs(real_min.y) > real_size)    real_size = fabs(real_min.y);
    if(fabs(real_max.z) > real_size)    real_size = fabs(real_max.z);
    if(fabs(real_min.z) > real_size)    real_size = fabs(real_min.z);
    real_size *= 2;
    if(real_size)
        real_scale = 65535.0 / real_size;
    else
        real_scale = 1.0;
    //------------------------------------------------------------------------
    for(j = 0; j < size(); j++)
    {
        if(at(j).size() > 2)
        {
            for(i = start; i < at(j).size(); i++) // ignore the origin vector
            {
                _x = (int)round((at(j)[i].x) * real_scale);
                _y = (int)round((at(j)[i].y) * real_scale);
                _z = (int)round((at(j)[i].z) * real_scale);
                if(_x >  32767) _x =  32767;    if(_x < -32767) _x = -32767;
                if(_y >  32767) _y =  32767;    if(_y < -32767) _y = -32767;
                if(_z >  32767) _z =  32767;    if(_z < -32767) _z = -32767;
                at(j)[i].x = (double)(_x);
                at(j)[i].y = (double)(_y);
                at(j)[i].z = (double)(_z);
            }
        }
    }
    //------------------------------------------------------------------------
    return;
}

//############################################################################
LaserBoy_real_segment_set& LaserBoy_real_segment_set::reverse()
{
    LaserBoy_real_segment_set reversed(p_space);
    if(size() > 1)
    {
        for(u_int i = 1; i <= size(); i++)
            reversed.push_back(at(size() - i));
        clear();
        insert(begin(), reversed.begin(), reversed.end());
    }
    return *this;
}

//############################################################################
void LaserBoy_real_segment_set::strip_color_rgb(const LaserBoy_color& c)
{
    for(u_int i = 0; i < size(); i++)
        at(i).strip_color_rgb(c);
}

//############################################################################
void LaserBoy_real_segment_set::sync_rgb_and_palette()
{
    for(u_int i = 0; i < size(); i++)
        at(i).sync_rgb_and_palette();
    return;
}

//############################################################################
//////////////////////////////////////////////////////////////////////////////
//############################################################################
