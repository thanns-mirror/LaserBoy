LaserBoy64.exe is a 64-bit Windows application linked to SDL2.dll

This means that SDL2.dll is the 64-bit version!
There is also a 32-bit version SDL2.dll. They are not interchangable.
If you are on a 64-bit Windows machine, you can copy SDL2.dll to C:\Windows\System32\.

LaserBoy.exe is a 32-bit Windows application linked to SDL.dll (SDL 1.2.15)
It was build in Windows XP and probably runs in previous versions of Windows.
This is an updated version of the same kind of exe that has always come in the LB zip.

This means that SDL.dll is the 32-bit version!

This 32-bit version of LaserBoy links with the 32-bit SDL.dll and runs in 64-bit Windows.

If you plan to build other Windows variations of LaserBoy from the src directory
You need to know that there are 32 and 64-bit versions of both SDL and SDL2.
Look at the Makefiles in the src dirctory.


Win32 XP and below    SDL2   Makefile.win
Win32 XP and below    SDL    Makefile_sdl1.win // used to build LaserBoy.exe
Win32 Vista and above SDL2   Makefile.w32
Win32 Vista and above SDL    Makefile_sdl1.w32
Win64                 SDL2   Makefile.w64      // used to build LaserBoy64.exe
Win64                 SDL    Makefile_sdl1.w64
Linux x86 PC          SDL2   Makefile
Linux x86 PC          SDL    Makefile_sdl1
Linux Raspberry Pi    SDL2   Makefile.rpi
Linux Raspberry Pi    SDL    Makefile_sdl1.rpi
MacOSX                SDL2   Makefile.osx      // not tested, needs editing

